#ifndef WORLD_H
#define WORLD_H

#include <iostream>

#include <QObject>
#include <QDialog>
#include <QtWidgets>

#include <QApplication>
#include <QMainWindow>
#include <memory>

#include "tile.h"
#include "creature.h"

struct CreatureLocation
{
    Tile tile;
    Creature creature;
};

class World : public QWidget
{
    Q_OBJECT

    QVector<std::shared_ptr<Tile>> m_tiles;
    QVector<std::shared_ptr<Creature>> m_creatures;

    QString m_dateFormat;

    quint16 m_w;
    quint16 m_h;

    QString m_fileName;

    //TODO: maybe include ingame time with the message
    QVector<QString> m_messageLog;

    quint64 m_time;

public:

    void generateNew(quint16 w, quint16 h, QJsonDocument save);
    quint32 getArea();

    std::shared_ptr<Creature> getPlayer();

    std::shared_ptr<Tile> getTile(quint32 number);
    std::shared_ptr<Tile> getTile(quint16 x, quint16 y);

    void log(QString message);

    void moveCreature(std::shared_ptr<Creature> creature, QVector3D direction);
    void moveCreature(std::shared_ptr<Creature> creature, GLfloat x, GLfloat y, GLfloat z=0.0f);

    World();
    quint16 getW() const;
    quint16 getH() const;

    QVector<QString> getMessageLog() const;

    //TODO: Maybe change the save format to a seed&player input based approach
    //      This way nothing is saved except for what the player does, and when loading,
    //      everything is simulated. Since the seed is stored, the random results will be the same every time
    void writeToFile();
    void loadFromFile(QString fileName);

    void setFileName(const QString &value);

    quint64 getTime() const;
    void setTime(const quint64 &time);

    QString getPlayerName();
    void setPlayerName(const QString &playerName);

    QString getPlayerBio();
    void setPlayerBio(const QString &playerBio);

    QVector<std::shared_ptr<Creature> > getCreatures() const;
    std::shared_ptr<Creature> getCreature(qint32 index);

    void passTime();

    void act(std::shared_ptr<Creature> creature);

signals:
    void messageLogged();
};

#endif // WORLD_H
