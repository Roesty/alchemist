#ifndef TITLEWINDOW_H
#define TITLEWINDOW_H

#include <iostream>

#include <QObject>
#include <QDialog>
#include <QtWidgets>

#include "newgame.h"
#include "loadgame.h"

QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
QT_END_NAMESPACE

class TitleWindow: public QDialog
{
    Q_OBJECT

private:
    QVBoxLayout* mainLayout;

    QLabel* title;

    enum {NEW_GAME=0, LOAD_GAME, OPTIONS, EXIT, MAX_BUTTONS};

    std::vector<QPushButton*> buttons;

    std::shared_ptr<NewGame> newGameWindow;
    std::shared_ptr<LoadGame> loadGameWindow;

    void disableWindow();

private slots:
    void createNewGame();
    void createLoadGame();
    void options();
    void exit();
    void enableWindow();

public:
    TitleWindow();
};

#endif // TITLEWINDOW_H
