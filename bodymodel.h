#ifndef BODYMODEL_H
#define BODYMODEL_H

#include <QObject>
#include <QTreeView>

#include "limb.h"
#include "creature.h"

class BodyModel: public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit BodyModel(QObject *parent = 0);
    ~BodyModel();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    void setupModelData(Creature* creature);
private:

    Limb* m_rootItem;
};
#endif // BODYMODEL_H
