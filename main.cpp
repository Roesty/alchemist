#include <QCoreApplication>
#include <QApplication>
#include <QSurfaceFormat>

#include "alchemist.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::cout<<"  float  : "<<sizeof(float)<<std::endl;
    std::cout<<"GLfloat  : "<<sizeof(GLfloat)<<std::endl;
    std::cout<<" qfloat16: "<<sizeof(qfloat16)<<std::endl;
    std::cout<<"QVector3D: "<<sizeof(QVector3D)<<std::endl;

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    Alchemist game;

    return a.exec();
}
