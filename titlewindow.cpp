#include "titlewindow.h"

void TitleWindow::disableWindow()
{
    using namespace std;
    cout<<"disabling Title Window"<<endl;
    setEnabled(false);
}

void TitleWindow::enableWindow()
{
    using namespace std;
    cout<<"enabling Title Window"<<endl;
    setEnabled(true);
}

void TitleWindow::createNewGame()
{
    using namespace std;

    newGameWindow.reset(new NewGame);
    newGameWindow->setWindowTitle("New Game");

    connect(newGameWindow.get(), SIGNAL(accepted()), this, SLOT(enableWindow()));
    connect(newGameWindow.get(), SIGNAL(rejected()), this, SLOT(enableWindow()));
    disableWindow();

    newGameWindow->show();
}

void TitleWindow::createLoadGame()
{
    using namespace std;

    loadGameWindow.reset(new LoadGame);
    loadGameWindow->setWindowTitle("Load Game");

    connect(loadGameWindow.get(), SIGNAL(accepted()), this, SLOT(enableWindow()));
    connect(loadGameWindow.get(), SIGNAL(rejected()), this, SLOT(enableWindow()));
    disableWindow();

    loadGameWindow->show();
}

void TitleWindow::options()
{
    using namespace std;
    cout<<"options clicked"<<endl;
}

void TitleWindow::exit()
{
    using namespace std;
    cout<<"exit clicked"<<endl;
    close();
}

TitleWindow::TitleWindow()
{
    title = new QLabel("Alchemist");
    title->setFont(QFont("Verdana", 100));

    for (int iii=0; iii<MAX_BUTTONS; iii++)
    {
        buttons.push_back(new QPushButton);
    }

    buttons[NEW_GAME]->setText("New game");
    buttons[LOAD_GAME]->setText("Load game");
    buttons[OPTIONS]->setText("Options");
    buttons[EXIT]->setText("Exit");

    mainLayout = new QVBoxLayout;
    setLayout(mainLayout);

    mainLayout->addWidget(title, 2);

    for (int iii=0; iii<MAX_BUTTONS; iii++)
    {
        mainLayout->addWidget(buttons[iii]);
    }

    connect(buttons[NEW_GAME], SIGNAL(clicked()), this, SLOT(createNewGame()));
    connect(buttons[LOAD_GAME], SIGNAL(clicked()), this, SLOT(createLoadGame()));
    connect(buttons[OPTIONS], SIGNAL(clicked()), this, SLOT(options()));
    connect(buttons[EXIT], SIGNAL(clicked()), this, SLOT(exit()));

    newGameWindow.reset();
}
