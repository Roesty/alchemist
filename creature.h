#ifndef CREATURE_H
#define CREATURE_H

#include <iostream>
#include <memory>
#include <QJsonObject>
#include <QJsonArray>
#include <QtGlobal>

#include "object.h"
#include "limb.h"

enum eCreatureType {
    CREATURE_PLAYER=0,
    CREATURE_RAT,
    CREATURE_MONKEY,
    MAX_CREATURE
};

/****************************************************************************************/
/* WARNING: CHANGING ENUM ORDER OR VALUES WILL BREAK SAVEGAMES THAT USES OTHER ORDERING */
/****************************************************************************************/
//TODO: remove the heavy reliance on enum ordering
enum eAITypes {

    //Controlled by the player (no AI)
    PLAYER_CONTROLLED=0,

    //Moves around randomly
    DOCILE,

    //Attacks the player
    //    AGRESSIVE,

    //Runs away from the player
    //    FLEEING,

    //Follows the player, but stays out of reach
    //    STALKING,

    MAX_AI_TYPES
};

/****************************************************************************************/
/* WARNING: CHANGING ENUM ORDER OR VALUES WILL BREAK SAVEGAMES THAT USES OTHER ORDERING */
/****************************************************************************************/
//TODO: remove the heavy reliance on enum ordering
enum eCreatureAttributes
{
    MOVEMENT_SPEED=0,

    MAX_CREATURE_ATTRIBUTES
};

class Creature: public Object
{
    quint32 m_ID;

    //Name of the creature
    QString m_name;

    //Bio of the creature
    QString m_bio;

    //TODO: Remove type variable
    eCreatureType m_type;

    //The limbs owned by this creature
    //Is a single limb as the brain is on top, this is a limitation might change later
    std::shared_ptr<Limb> m_body;

    eAITypes m_AI;

    quint32 m_tileID;

    QVector<qint32> m_attributes;

    qint32 m_cooldown;

public:
    Creature();

    void buildCreature(eCreatureType type);

    quint32 ID() const;
    void setID(const quint32 &ID);

    QJsonObject serialized();
    bool fromSerialized(QJsonObject json);

    eCreatureType type() const;
    void setType(const eCreatureType &type);

    void translateModel(QVector3D translation);

    void setZ(GLfloat z);

    eAITypes AI() const;
    void setAI(const eAITypes &AI);

    quint32 tileID() const;
    void setTileID(const quint32 &tileID);

    QVector<qint32> getAttributes() const;
    void setAttributes(const QVector<qint32> &attributes);

    qint32 getAttribute(eCreatureAttributes attribute);
    void setAttribute(eCreatureAttributes attribute, qint32 value);

    qint32 cooldown() const;
    void setCooldown(const qint32 &cooldown);
    void reduceCooldown();
    bool canMove();
    void hasMoved();

    QString name() const;
    void setName(const QString &name);
    QString getBio() const;
    void setBio(const QString &bio);

    std::shared_ptr<Limb> getBody() const;
    void setBody(const std::shared_ptr<Limb> &body);

    bool isPlayer();
};

#endif // CREATURE_H
