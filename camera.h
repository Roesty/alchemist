#ifndef CAMERA_H
#define CAMERA_H

#include <QWidget>
#include <QtOpenGL/QGL>
#include <QObject>
#include <QDialog>
#include <QtWidgets>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include <iostream>

class Camera
{
public:

    enum eProjection {
        PERSPECTIVE=0,
        ORTHOGRAPHIC
    };

    enum eDirection {
        FORWARD=0,
        BACKWARD,
        RIGHT,
        LEFT,
        UP,
        DOWN
    };

    Camera();

    void setProjectionType(eProjection projection);
    eProjection getProjectionType();

    QMatrix4x4 getProjection();
    QMatrix4x4 getView();

    void setPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
    void setOrthographic(QMatrix4x4 orthographic);

    QVector3D getPosition();
    QVector3D getDirection();

    void setToDefault();

    void translate(QVector3D translation);

    void resetPerspective(float fov, float window_w, float window_h, float near, float far);
    void updateWindowSize(GLuint  window_w, GLuint window_h);

    QVector3D getUp() const;
    void setUp(const QVector3D &up);

    void moveInDirection(eDirection direction, float scalar=1.0f);

    void setX(GLfloat x);
    void setY(GLfloat y);
    void setZ(GLfloat z);

    void setDirection(QVector3D direction);

    void modifyZoom(GLfloat amount);

    void setPosition(const QVector3D &position);

private:

    eProjection m_curProjection;
    QMatrix4x4 m_projection;

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    QMatrix4x4 m_perspective;
    //In world coordinates
    QMatrix4x4 m_orthographic;

    QVector3D m_position;

    QVector3D forward();
    QVector3D right();
    QVector3D up();

    void recalculatePerspective();

    //projection stuff
    GLfloat m_fov;
    GLfloat m_w;
    GLfloat m_h;
    GLfloat m_near;
    GLfloat m_far;

    GLfloat m_zoom;
    GLfloat m_zoomMin;
    GLfloat m_zoomMax;

    GLfloat m_fovMin;
    GLfloat m_fovMax;

    float m_moveSpeed;

};

#endif // CAMERA_H
