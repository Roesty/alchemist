#include "alchemist.h"

void Alchemist::enableWindow()
{
    using namespace std;
    cout<<"enabling Title Window"<<endl;
    setEnabled(true);
}

void Alchemist::disableWindow()
{
    using namespace std;
    cout<<"disabling Title Window"<<endl;
    setEnabled(false);
}

void Alchemist::loadGame(QString fileName)
{
    using namespace std;
    if (m_world.get())
    {
        //TODO: add save current game prompt before loading if a world is already loaded
        cout<<"There is already a world loaded. Unloading now."<<endl;
        unloadGame();
    }

    cout<<fileName.toStdString()<<endl;

    m_world.reset(new World);
    m_world->loadFromFile(fileName);

    m_player = m_world->getPlayer();

    distributeWorld();

    play();
}

Alchemist::eGameState Alchemist::state() const
{
    return m_state;
}

void Alchemist::setState(const eGameState &state)
{
    m_state = state;
}

void Alchemist::play()
{
    //Pass time until the player can move
    bool playerCanMove = false;

    while (!playerCanMove)
    {
        if (m_player->canMove())
        {
            playerCanMove=true;
            break;
        }

        //If the player could not move, pass the time
        m_world->passTime();

        //If the player could not move this turn, then pass time normally for all the creatures
        //all the creatures include the player. This also reduces the cooldown for all creatures
        for (qint32 iii=0; iii<m_world->getCreatures().size(); iii++)
        {
            std::shared_ptr<Creature> creature=m_world->getCreature(iii);

            if (creature->canMove())
            {
                m_world->act(creature);
            }
            else
            {
                creature->reduceCooldown();
            }
        }
    }

    m_info->updateDisplay();
}

void Alchemist::setupGeneral()
{
    //Seed the random generator with the current time
    qsrand(QDateTime::currentMSecsSinceEpoch()%UINT_MAX);
//    qsrand(0);

    setState(PLAYING);
}

void Alchemist::setupMenu()
{
    //Setup the actions
    m_newSave = new QAction("New", this);
    m_newSave->setStatusTip("Create a new save");
    connect(m_newSave, SIGNAL(triggered()), this, SLOT(openNewGame()));

    m_loadSave = new QAction("Load", this);
    m_loadSave->setStatusTip("Load a save");
    connect(m_loadSave, SIGNAL(triggered()), this, SLOT(openLoadGame()));

    m_unloadSave = new QAction("Unload", this);
    m_unloadSave->setStatusTip("Unload the current save. Debug feature");
    connect(m_unloadSave, SIGNAL(triggered()), this, SLOT(unloadGame()));

    //Setup the help action
    m_getHelp = new QAction("Get help", this);
    m_getHelp->setStatusTip("Shows the help menu");
    connect(m_getHelp, SIGNAL(triggered()), this, SLOT(showHelp()));

    m_menu = menuBar();

    m_saveMenu = new QMenu("Game", m_menu);
    m_saveMenu->addAction(m_newSave);
    m_saveMenu->addAction(m_loadSave);
    m_saveMenu->addAction(m_unloadSave);
    m_menu->addMenu(m_saveMenu);

    m_helpMenu = new QMenu("Help", m_menu);
    m_helpMenu->addAction(m_getHelp);
    m_menu->addMenu(m_helpMenu);

    m_menu->show();
}

void Alchemist::setupWindow()
{
    ///Info widget
    m_info = new Info(this);
    m_info->setMinimumSize(100, 50);
    m_info->setMaximumSize(300, 50);

    ///Bodyview widget
    m_bodyview = new BodyView(this);
    m_bodyview->setMaximumWidth(300);

    ///Renderarea widget
    m_renderArea = new RenderArea(this);

    ///Gamelog widget
    m_log = new Log(this);
    m_log->setMinimumHeight(50);
    m_log->setMaximumHeight(150);

    ///Layout setup
    m_leftPanel = new QGridLayout(this);
    m_leftPanel->addWidget(m_info, 0, 0);
    m_leftPanel->addWidget(m_bodyview, 1, 0);
    m_leftPanel->maximumSize().setWidth(300);

    m_rightPanel = new QGridLayout(this);
    m_rightPanel->addWidget(m_renderArea, 0, 0, 1, 3);
    m_rightPanel->addWidget(m_log, 1, 0);

    //Assign the panels to the layout
    m_layout = new QGridLayout(this);
    m_layout->addLayout(m_leftPanel, 0, 0);
    m_layout->addLayout(m_rightPanel, 0, 1);

    ///Since this is a mainwindow, set up the central widget to be the layout
    m_centralWidget = new QWidget(this);
    m_centralWidget->setLayout(m_layout);
    setCentralWidget(m_centralWidget);
}

void Alchemist::setupConnections()
{
}

void Alchemist::distributeWorld()
{
    m_log->setWorld(m_world);
    m_info->setWorld(m_world);
    m_bodyview->setWorld(m_world);
    m_renderArea->setWorld(m_world);
}

void Alchemist::openNewGame()
{
    newGameWindow.reset(new NewGame);
    newGameWindow->setWindowTitle("New Game");

    connect(newGameWindow.get(), SIGNAL(accepted()), this, SLOT(enableWindow()));
    connect(newGameWindow.get(), SIGNAL(rejected()), this, SLOT(enableWindow()));
    disableWindow();

    newGameWindow->show();
}

void Alchemist::openLoadGame()
{
    loadGameWindow.reset(new LoadGame);
    loadGameWindow->setWindowTitle("Load Game");

    connect(loadGameWindow.get(), SIGNAL(accepted()), this, SLOT(enableWindow()));
    connect(loadGameWindow.get(), SIGNAL(rejected()), this, SLOT(enableWindow()));
    connect(loadGameWindow.get(), SIGNAL(loadThisSave(QString)), this, SLOT(loadGame(QString)));
    disableWindow();

    loadGameWindow->show();
}

void Alchemist::unloadGame()
{
    std::cout<<"Unload game"<<std::endl;

    if (m_world.get())
    {
        m_world->writeToFile();

        m_world = nullptr;
        distributeWorld();
    }
    else
    {
        m_log->insertMessage("Nothing to unload, no world loaded.");
    }
}

void Alchemist::showHelp()
{
    QMessageBox helpBox;
    QString helpMessage = "";
    helpMessage += "WASD or 1-9 to move\n";
    helpMessage += "x to look around\n";
    helpBox.setText(helpMessage);
    helpBox.setButtonText(QDialogButtonBox::Ok, "Thanks");
    helpBox.exec();
}

void Alchemist::keyPressEvent(QKeyEvent *event)
{
    switch (state()) {
    case PLAYING:
    {
        switch (event->key())
        {
        case Qt::Key_X:
        {
            setState(LOOK_AROUND);
            break;
        }
        case Qt::Key_1:
        {
            movePlayer(-1, -1);
            break;
        }
        case Qt::Key_2:
        case Qt::Key_S:
        {
            movePlayer(0, -1);
            break;
        }
        case Qt::Key_3:
        {
            movePlayer(1, -1);
            break;
        }
        case Qt::Key_4:
        case Qt::Key_A:
        {
            movePlayer(-1, 0);
            break;
        }
        case Qt::Key_6:
        case Qt::Key_D:
        {
            movePlayer(1, 0);
            break;
        }
        case Qt::Key_7:
        {
            movePlayer(-1, 1);
            break;
        }
        case Qt::Key_8:
        case Qt::Key_W:
        {
            movePlayer(0, 1);
            break;
        }
        case Qt::Key_9:
        {
            movePlayer(1, 1);
            break;
        }
        case Qt::Key_Plus:
        {
            m_renderArea->modifyZoom(-1);
            break;
        }
        case Qt::Key_Minus:
        {
            m_renderArea->modifyZoom(1);
            break;
        }
        }
        break;
    }
    case LOOK_AROUND:
    {
        break;
    }

    }

}

void Alchemist::wheelEvent(QWheelEvent *event)
{
    m_renderArea->modifyZoom(event->angleDelta().ry());
}

void Alchemist::movePlayer(QVector3D direction)
{
    if (m_world.get())
    {
        m_world->moveCreature(m_player, direction);
        play();
    }
    else
    {
        std::cout<<"Cant move player, there is no world loaded"<<std::endl;
        m_log->insertMessage("There is no world loaded.");
    }
}

void Alchemist::movePlayer(GLfloat x, GLfloat y, GLfloat z)
{
    movePlayer(QVector3D(x, y, z));
}

Alchemist::Alchemist()
{
    //Setup general stuff
    setupGeneral();

    //Setup the main game view
    setupWindow();

    //Setup the top menu view
    setupMenu();

    //Setup the connections between the widgets
    setupConnections();

    //Show both the central widget and the layout
    centralWidget()->show();

    //Start in maximised window
        showMaximized();
//    show();

    /*
    mainWindow->resize(500, 500);
    mainWindow->setMinimumSize(100, 100);
    mainWindow->setMaximumSize(1000, 1000);

    mainWindow->show();

    titleWindow.reset(new TitleWindow);
    titleWindow->setWindowTitle("Title Window");
    titleWindow->show();
*/

    // Automatically load a game for quicker debugging
//            loadGame("saves/new game test.json");
    loadGame("saves/new game test.json");

//    play();
}




















































