#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QtOpenGL/QGL>
#include <QObject>
#include <QDialog>
#include <QtWidgets>
#include <memory>
#include <iostream>

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include "world.h"
#include "camera.h"

//QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
//QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class RenderArea : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

    //OpenGL program and context
    std::shared_ptr<QOpenGLShaderProgram> m_program;
    std::unique_ptr<QOpenGLContext> m_openGLContext;

    QVector<Vertex> m_allVertices;

    QOpenGLVertexArrayObject m_VAO;
    QOpenGLBuffer m_VBO;

    //Background color. Is not seen if not at the end of the map
    QColor m_clearColor;

    //Removes all loaded textures and vertices
    void clean();

    //Clears the renderarea
    void clear();

    //Initialises OpenGL
    void initializeGL() override;

    //Draw the world
    void paintGL() override;

    //Called on resize of window
    void resizeGL(int width, int height) override;

    //Called when the widget is clicked
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    //Creates a 3d world from the map state
    void determineModels();

    //Creates models
    void buildModels();

    //Creates a cube
    void buildCube(std::shared_ptr<Model> model, float texID);

    //The world that is rendered
    std::shared_ptr<World> m_world;

    //The camera used for rendering
    Camera m_camera;

    //Colors used for rendering
    QVector<QColor> m_colors;

    //Textures for rendering
    QVector<std::shared_ptr<QOpenGLTexture>> m_textures;
    QVector<std::shared_ptr<Model>> m_models;

    //Scale of the map
    GLfloat m_scaleX;
    GLfloat m_scaleY;

    void moveCameraToCreature(std::shared_ptr<Creature> creature);

public:
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    explicit RenderArea(QWidget *parent = nullptr);
    ~RenderArea();

    void setWorld(const std::shared_ptr<World> &world);

    //Controls for the camera
    void modifyZoom(qint8 amount);
    void moveCamera(QVector2D movement);
    void moveCamera(qint16 x, qint16 y);

signals:

public slots:
protected:
};

#endif // RENDERAREA_H
