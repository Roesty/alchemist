#ifndef OBJECT_H
#define OBJECT_H

#include "model.h"

#include <QFloat16>

class Object
{
private:

protected:
    QVector3D m_position;
    std::shared_ptr<Model> m_model;

public:
    Object();

    std::shared_ptr<Model> model() const;
    void setModel(const std::shared_ptr<Model> &model);

    QVector3D position() const;
    void setPosition(const QVector3D &position);
    void setPosition(qfloat16 x, qfloat16 y, qfloat16 z);
};

#endif // OBJECT_H
