#include "loadgame.h"

void LoadGame::showHelp()
{
    QMessageBox helpBox;
    helpBox.setText("Enter your character name and select the race and class. The bio is optional and purely cosmetic.");
    helpBox.setButtonText(QDialogButtonBox::Ok, "Thanks");
    helpBox.exec();
}

void LoadGame::loadSave()
{
    QModelIndex selectedSave = savesView->currentIndex();

    QString fileName = savesView->model()->data(savesView->model()->index(selectedSave.row(), FILE_NAME)).toString();

    emit loadThisSave(fileName);

    accept();
}

void LoadGame::saveSelected()
{
    QModelIndex selected = savesView->currentIndex();

    QString bio = savesView->model()->data(savesView->model()->index(selected.row(), BIO)).toString();
    QString name = savesView->model()->data(savesView->model()->index(selected.row(), CHAR_NAME)).toString();

    selectedCharacterName->setText(name);
    selectedCharacterBio->setText(bio);
}

LoadGame::LoadGame()
{
    saves.reset(new QStandardItemModel(0, 5));
    setMinimumSize(1000, 800);

    QDir saveFolder("saves");

    //Get all the json files in this directory
    QStringList saveNames = saveFolder.entryList(QStringList() <<"*.json", QDir::Files);

    foreach (QString saveName, saveNames)
    {
        QString fileName = saveFolder.filePath(saveName);

        QFile save(fileName);

        if (!save.open(QIODevice::ReadOnly))
        {
            std::cout<<"Could not open file"<<std::endl;
            return;
        }

        QByteArray saveData = save.readAll();

        QJsonDocument jsonSave = QJsonDocument::fromJson(saveData);

        saves->insertRow(0);
        saves->setData(saves->index(0, FILE_NAME), fileName);

        saves->setData(saves->index(0, CHAR_NAME), jsonSave["name"].toString());
        saves->setData(saves->index(0, LAST_PLAYED), jsonSave["last played"].toString());

        QString newData = "";
        if (jsonSave["new"].toBool())
        {
            newData = "NEW";
        }

        saves->setData(saves->index(0, NEW), newData);

        saves->setData(saves->index(0, BIO), jsonSave["bio"].toString());
    }

    //Create the saves view, this is a treeview that holds all the saves
    savesView = new QTreeView;

    //Give the columns proper namess
    saves->setHeaderData(FILE_NAME, Qt::Horizontal, "File");
    saves->setHeaderData(CHAR_NAME, Qt::Horizontal, "Name");
    saves->setHeaderData(LAST_PLAYED, Qt::Horizontal, "Last played");
    saves->setHeaderData(NEW, Qt::Horizontal, "New");
    saves->setHeaderData(BIO, Qt::Horizontal, "Bio");

    //Configure the view
    savesView->setRootIsDecorated(false);
    savesView->setAlternatingRowColors(true);
    savesView->setSortingEnabled(true);
    savesView->setItemsExpandable(false);
    savesView->header()->setStretchLastSection(true);

    //Insert all the models
    savesView->setModel(saves.get());

    //Hide the filename and bio column
    savesView->setColumnHidden(FILE_NAME, true);
    savesView->setColumnHidden(BIO, true);

    //TODO: automatically resize to optimal width
    savesView->setColumnWidth(CHAR_NAME, 200);
    savesView->setColumnWidth(LAST_PLAYED, 200);
    savesView->setColumnWidth(NEW, 50);

    //Make the view uneditable
    savesView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(savesView, SIGNAL(clicked(QModelIndex)), this, SLOT(saveSelected()));

    selectedCharacterName = new QLineEdit();
    selectedCharacterName->setReadOnly(true);
    selectedCharacterNameLabel = new QLabel("Character name:");

    selectedCharacterBio = new QTextEdit();
    selectedCharacterBio->setReadOnly(true);
    selectedCharacterBioLabel = new QLabel("Bio:");

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Help |
                                     QDialogButtonBox::Ok |
                                     QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(loadSave()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(buttonBox, SIGNAL(helpRequested()), this, SLOT(showHelp()));

    mainLayout = new QGridLayout;
    mainLayout->addWidget(savesView, 0, 0, 4, 1);

    mainLayout->addWidget(selectedCharacterNameLabel, 0, 1);
    mainLayout->addWidget(selectedCharacterName, 1, 1);

    mainLayout->addWidget(selectedCharacterBioLabel, 2, 1);
    mainLayout->addWidget(selectedCharacterBio, 3, 1);

    mainLayout->addWidget(buttonBox, 4, 0, 1, 2);

    setLayout(mainLayout);

//    savesView->clicked();
}

































