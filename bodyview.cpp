#include "bodyview.h"

void BodyView::setWorld(const std::shared_ptr<World> &world)
{
    m_world = world;
    if (m_world.get())
    {
        setPlayer(m_world->getPlayer());
        readPlayer();
        m_view->expandAll();
    }
    else
    {
        m_model.reset();
    }
}

std::shared_ptr<Creature> BodyView::player() const
{
    return m_player;
}

void BodyView::setPlayer(const std::shared_ptr<Creature> &player)
{
    m_player = player;
}

/*
void BodyView::addLimb(std::shared_ptr<Limb> limb, quint32 depth)
{
//    m_bodyData->insertRow(0);
//    m_bodyData->setData(m_bodyData->index(0, NAME), limb->name());
//    m_bodyData->setData(m_bodyData->index(0, HP), limb->hp());

    for (quint32 indent=0; indent<depth; indent++)
    {
        std::cout<<"  ";
    }
    std::cout<<limb->name().toStdString()<<std::endl;
    for (std::shared_ptr<Limb> appendage: limb->appendages())
    {
        addLimb(appendage, depth+1);
    }
}
*/

void BodyView::readPlayer()
{
    if (m_player.get())
    {
        m_model = std::shared_ptr<BodyModel>(new BodyModel(this));
        m_model->setupModelData(m_player.get());
        m_view->setModel(m_model.get());

        //TODO: automatically resize to optimal width
        qint32 hpWidth = 40;
        m_view->setColumnWidth(HP, hpWidth);
        m_view->setColumnWidth(NAME, m_view->width()-hpWidth);

        //        m_bodyData->clear();
        //        addLimb(player()->getBody(), 0);
    }

    //    m_view->setModel(m_bodyData.get());

}

void BodyView::mousePressEvent(QMouseEvent *event)
{
    m_view->clearFocus();
}

void BodyView::mouseReleaseEvent(QMouseEvent *event)
{
    m_view->clearFocus();
}

void BodyView::resizeEvent(QResizeEvent *event)
{
    qint32 hpWidth = 40;
    m_view->setColumnWidth(HP, hpWidth);
    m_view->setColumnWidth(NAME, m_view->width()-hpWidth);
}

BodyView::BodyView(QWidget *parent)
{
    setParent(parent);
//    m_model = std::shared_ptr<BodyModel>(new BodyModel(this));

    m_view = new QTreeView(this);

    m_mainLayout = new QGridLayout;
    m_mainLayout->addWidget(m_view, 0, 0);

    setLayout(m_mainLayout);
    //    m_bodyData.reset(new QStandardItemModel(0, 2));

    //    m_bodyData->setHeaderData(NAME, Qt::Horizontal, "Name");
    //    m_bodyData->setHeaderData(HP, Qt::Horizontal, "HP");

    /*
    m_view->setRootIsDecorated(false);
    m_view->setAlternatingRowColors(true);
    m_view->setSortingEnabled(true);
    m_view->setItemsExpandable(false);
*/


    m_view->setIndentation(10);
    m_view->expandAll();

    //Make the view uneditable
    m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_view->setFocusPolicy(Qt::NoFocus);

    show();
}

