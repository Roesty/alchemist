#include "bodymodel.h"

BodyModel::BodyModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    rootData << "Name" << "HP";
    m_rootItem = new Limb;
    m_rootItem->setToHeader();
}

BodyModel::~BodyModel()
{
    delete m_rootItem;
}

int BodyModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<Limb*>(parent.internalPointer())->columnCount();
    else
        return m_rootItem->columnCount();
}

QVariant BodyModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    Limb *limb = static_cast<Limb*>(index.internalPointer());

    return limb->data(index.column());
}

Qt::ItemFlags BodyModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant BodyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return m_rootItem->data(section);

    return QVariant();
}

QModelIndex BodyModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    Limb* parentItem;

    if (!parent.isValid())
        parentItem = m_rootItem;
    else
        parentItem = static_cast<Limb*>(parent.internalPointer());

    Limb* childItem = parentItem->appendagePtr(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex BodyModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    Limb* childItem = static_cast<Limb*>(index.internalPointer());
    Limb* parentItem = childItem->parent();

    if (parentItem == m_rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int BodyModel::rowCount(const QModelIndex &parent) const
{
    Limb* parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = m_rootItem;
    else
        parentItem = static_cast<Limb*>(parent.internalPointer());

    return parentItem->appendageCount();
}

void BodyModel::setupModelData(Creature* creature)
{
    if (creature)
    {
        if (creature->getBody().get())
        {
            m_rootItem->attachLimb(creature->getBody());
        }
        else
        {
            std::cout<<"No body"<<std::endl;
        }
    }
    else
    {
        std::cout<<"No player"<<std::endl;
    }
}




















































