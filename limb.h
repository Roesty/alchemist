#ifndef LIMB_H
#define LIMB_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QVector>
#include <QJsonObject>
#include <QJsonArray>

#include <iostream>
#include <memory>

/****************************************************************************************/
/* WARNING: CHANGING ENUM ORDER OR VALUES WILL BREAK SAVEGAMES THAT USES OTHER ORDERING */
/****************************************************************************************/
//TODO: remove the heavy reliance on enum ordering
enum eLimbAttributes
{
    NAME=0,
    HP,
    ARMOR,
    WEIGHT,
    SIZE,

    MAX_LIMB_ATTRIBUTES
};

enum eLimbAbilites
{
    //Punching, kicking, tackling...
    CAN_HIT=0,

    //Slashing, stabbing...
    CAN_CUT,

    //Biting, stomping...
    CAN_CRUSH,

    MAX_LIMB_ABILITES
};

class Limb
{
    //The limb this limb is attached to
    Limb* m_parent;

    //The limbs attached to this limb
    QVector<std::shared_ptr<Limb>> m_appendages;

    //List over the attributes
    QVector<QVariant> m_attributes;

    //List over the abilites
    QVector<bool> m_abilites;

public:

    Limb(QString name="");
    ~Limb();

    void attachLimb(QString name);
    void attachLimb(std::shared_ptr<Limb> limb);

    std::shared_ptr<Limb> appendage(qint32 row);
    Limb* appendagePtr(qint32 row);
    qint32 appendageCount() const;
    qint32 columnCount() const;
    QVariant data(int column) const;
    int row() const;

    Limb* parent() const;
    void setParent(const std::shared_ptr<Limb> &parent);
    void setParent(Limb* parent);

    QJsonObject serialized();
    void fromSerialized(QJsonObject json);

    QString name() const;
    void setName(const QString &name);

    qint32 hp() const;
    void setHp(const qint32 &hp);

    QVector<std::shared_ptr<Limb>> appendages() const;

    void setToHeader();
};

#endif // LIMB_H
