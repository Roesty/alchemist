#include "renderarea.h"

void RenderArea::clean()
{
    m_allVertices.clear();
    m_textures.clear();
    m_VAO.destroy();
    m_VBO.destroy();
    m_world.reset();
    m_models.clear();
}

void RenderArea::clear()
{
}

RenderArea::RenderArea(QWidget *parent) : QOpenGLWidget(parent)
{
    QSurfaceFormat format;
    format.setMajorVersion( 4 );
    format.setMinorVersion( 1 );
    format.setProfile( QSurfaceFormat::CompatibilityProfile );

    m_openGLContext.reset(new QOpenGLContext);
    m_openGLContext->create();
    //    m_openGLContext->makeCurrent();

    m_clearColor.setRgb(64, 64, 64, 1.0);
    //        m_clearColor.setRgb(255, 0, 255);
    //    m_clearColor.setRgb(32, 32, 40);
    //    m_clearColor.setRgb(32, 32, 40);

    m_colors = QVector<QColor>(MAX_COLOR);
    m_colors[WALKABLE].setRgb(255, 255, 255);
    m_colors[UNWALKABLE].setRgb(255, 255, 255);

    m_scaleX=1.0f;
    m_scaleY=1.0f;

    memset(m_textures.begin(), 0, sizeof(QOpenGLTexture)*m_textures.size());
}

RenderArea::~RenderArea()
{
}

void RenderArea::initializeGL()
{
    initializeOpenGLFunctions();

    std::shared_ptr<QOpenGLTexture> newTexture;
    newTexture.reset(new QOpenGLTexture(
                         QImage("/home/lace/QtProjects/Alchemist/Alchemist/textures/good/floor_64.jpg").mirrored()));
    newTexture->create();
    m_textures.push_back(newTexture);

    newTexture.reset(new QOpenGLTexture(
                         QImage("/home/lace/QtProjects/Alchemist/Alchemist/textures/good/wall2_64.jpg").mirrored()));
    newTexture->create();
    m_textures.push_back(newTexture);

    newTexture.reset(new QOpenGLTexture(
                         QImage("/home/lace/QtProjects/Alchemist/Alchemist/textures/good/player_at.png").mirrored()));
    newTexture->create();
    m_textures.push_back(newTexture);

    newTexture.reset(new QOpenGLTexture(
                         QImage("/home/lace/QtProjects/Alchemist/Alchemist/textures/good/rat.png").mirrored()));
    newTexture->create();
    m_textures.push_back(newTexture);

    newTexture.reset(new QOpenGLTexture(
                         QImage("/home/lace/QtProjects/Alchemist/Alchemist/textures/good/monkey.png").mirrored()));
    newTexture->create();
    m_textures.push_back(newTexture);

    glEnable(GL_DEPTH_TEST);
    //    glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);

    //TODO: Move shader source to separate files and give variables consistent naming style
    QOpenGLShader *vshader = new QOpenGLShader(QOpenGLShader::Vertex, this);
    const char *vsrc =
            "attribute highp vec4 vertex;                      \n"
            "attribute mediump vec4 texCoord;                  \n"
            "attribute mediump float texID;                    \n"
            "attribute lowp vec4 colAttr;                      \n"
            "                                                  \n"
            "varying mediump vec4 texc;                        \n"
            "varying mediump float texid;                      \n"
            "varying lowp vec4 color;                          \n"
            "                                                  \n"
            "uniform mediump mat4 matrix;                      \n"
            "uniform mediump mat4 translation;                 \n"
            "                                                  \n"
            "void main(void)                                   \n"
            "{                                                 \n"
            "    gl_Position = matrix * vertex;                \n"
            "    texc = texCoord;                              \n"
            "    texid = texID;                                \n"
            "    color = colAttr;                              \n"
            "}                                                 \n";

    vshader->compileSourceCode(vsrc);

    QOpenGLShader *fshader = new QOpenGLShader(QOpenGLShader::Fragment, this);
    const char *fsrc =
            "uniform sampler2D textureFloor;                                 \n"
            "uniform sampler2D textureWall;                                  \n"
            "uniform sampler2D texturePlayer;                                \n"
            "uniform sampler2D textureRat;                                   \n"
            "uniform sampler2D textureMonkey;                                \n"
            "                                                                \n"
            "varying mediump vec4 texc;                                      \n"
            "varying mediump float texid;                                    \n"
            "varying lowp vec4 color;                                        \n"
            "                                                                \n"
            "void main(void)                                                 \n"
            "{                                                               \n"
            "    if (texid == 0)                                             \n"
            "    {                                                           \n"
            "        gl_FragColor = texture2D(textureFloor,  texc.st)*color; \n"
            "    }                                                           \n"
            "                                                                \n"
            "    if (texid == 1)                                             \n"
            "    {                                                           \n"
            "        gl_FragColor = texture2D(textureWall,   texc.st)*color; \n"
            "    }                                                           \n"
            "                                                                \n"
            "    if (texid == 2)                                             \n"
            "    {                                                           \n"
            "        gl_FragColor = texture2D(texturePlayer, texc.st)*color; \n"
            "    }                                                           \n"
            "                                                                \n"
            "    if (texid == 3)                                             \n"
            "    {                                                           \n"
            "        gl_FragColor = texture2D(textureRat,    texc.st)*color; \n"
            "    }                                                           \n"
            "                                                                \n"
            "    if (texid == 4)                                             \n"
            "    {                                                           \n"
            "        gl_FragColor = texture2D(textureMonkey, texc.st)*color; \n"
            "    }                                                           \n"
            "}                                                               \n";

    fshader->compileSourceCode(fsrc);

    m_program.reset(new QOpenGLShaderProgram);
    m_program->addShader(vshader);
    m_program->addShader(fshader);

    m_program->bindAttributeLocation("vertex",   POSITION);
    m_program->bindAttributeLocation("texCoord", TEX_POSITION);
    m_program->bindAttributeLocation("texID",    TEX_ID);
    m_program->bindAttributeLocation("colAttr",  COLOR);
    m_program->link();

    m_program->bind();

    //TODO: Move textures into texture array
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_textures[TEX_FLOOR]->textureId());
    m_program->setUniformValue(m_program->uniformLocation("textureFloor"), TEX_FLOOR);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_textures[TEX_WALL]->textureId());
    m_program->setUniformValue(m_program->uniformLocation("textureWall"), TEX_WALL);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, m_textures[TEX_PLAYER]->textureId());
    m_program->setUniformValue(m_program->uniformLocation("texturePlayer"), TEX_PLAYER);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, m_textures[TEX_RAT]->textureId());
    m_program->setUniformValue(m_program->uniformLocation("textureRat"), TEX_RAT);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, m_textures[TEX_MONKEY]->textureId());
    m_program->setUniformValue(m_program->uniformLocation("textureMonkey"), TEX_MONKEY);


    //TODO: figure out why I have to set the active texture to an unused number to make all textures show up
    //      If i dont do this, the last active texture will not appear when it should
    glActiveTexture(GL_TEXTURE31);

    //Set the clear color
    glClearColor(static_cast<GLclampf>(m_clearColor.redF()),
                 static_cast<GLclampf>(m_clearColor.greenF()),
                 static_cast<GLclampf>(m_clearColor.blueF()),
                 static_cast<GLclampf>(m_clearColor.alphaF()));

    //Build the models
    buildModels();
}

void RenderArea::moveCameraToCreature(std::shared_ptr<Creature> creature)
{
    //Move the camera in the opposite direction of the player
    m_camera.setPosition(-creature->position());

    //Move it a half unit to center the player
    m_camera.setX(m_camera.getPosition().x()-0.5);
    m_camera.setY(m_camera.getPosition().y()-0.5);

    //Move the camera backwards
    m_camera.setZ(-5);
}

void RenderArea::paintGL()
{
    if (m_world.get())
    {
        m_VAO.bind();
        m_VBO.bind();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_program->bind();

        //Go trough the map to find the tile holding the player
        for (std::shared_ptr<Creature> creature: m_world->getCreatures())
        {
            if (creature->AI() == PLAYER_CONTROLLED)
            {
                moveCameraToCreature(creature);
            }

            //replace the creature's vertices in the vbo
            glBufferSubData(
                        GL_ARRAY_BUFFER,
                        creature->model()->m_meshes[0].start(),
                    creature->model()->m_meshes[0].vertices().count()*sizeof(Vertex),
                    creature->model()->m_meshes[0].vertices().constData());
        }

        m_program->setUniformValue("matrix", m_camera.getView());

        glDrawArrays(GL_TRIANGLES, 0, m_allVertices.size());

        update();
    }
}

void RenderArea::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    m_camera.updateWindowSize(width, height);
}

void RenderArea::mousePressEvent(QMouseEvent *event)
{
    update();
}

void RenderArea::mouseReleaseEvent(QMouseEvent *event)
{
}

void RenderArea::determineModels()
{
    //Before determining vertices, remove all the current vertices
    m_allVertices.clear();

    for (quint16 y=0; y<m_world->getH(); y++)
    {
        for (quint16 x=0; x<m_world->getW(); x++)
        {
            //Get the tile at this position
            std::shared_ptr<Tile> currentTile = m_world->getTile(x, y);

            //            std::cout<<currentTile->position().x()<<" - "<<currentTile->position().y()<<std::endl;

            //If the tile is walkable make it floor, if not make it a wall
            if (currentTile->isWalkable())
            {
                //                std::cout<<"walkable"<<std::endl;
                currentTile->setModel(m_models[MODEL_FLOOR]->getCopy());
            }
            else
            {
                //                std::cout<<"not walkable"<<std::endl;
                currentTile->setModel(m_models[MODEL_WALL]->getCopy());
            }

            //Set the start of the model to be the current size of all the vertices placed so far
            currentTile->model()->m_meshes[0].setStart(m_allVertices.count()*sizeof(Vertex));

            //Go through each vertex in this model and add it to the giant vector
            for (quint64 iii = 0; iii < currentTile->model()->m_meshes[0].size(); iii++)
            {
                m_allVertices.append(currentTile->model()->m_meshes[0].vertices()[iii]);
            }

            std::shared_ptr<Creature> creature = currentTile->creature();

            if (creature.get())
            {
                //                std::cout<<"Tile at "<<x<<","<<y<<" has creature"<<std::endl;

                switch (creature->type())
                {
                case CREATURE_PLAYER:
                {
                    creature->setModel(m_models[MODEL_PLAYER]->getCopy());

                    //                    std::cout<<"Vertices in player model: "<<creature->model()->m_meshes[0].modelVertices().count()<<std::endl;
                    break;
                }
                case CREATURE_RAT:
                {
                    creature->setModel(m_models[MODEL_RAT]->getCopy());

                    //                    std::cout<<"Vertices in rat model: "<<creature->model()->m_meshes[0].modelVertices().count()<<std::endl;
                    break;
                }
                case CREATURE_MONKEY:
                {
                    creature->setModel(m_models[MODEL_MONKEY]->getCopy());
                    break;
                }
                case MAX_CREATURE:
                {
                    break;
                }
                }

                creature->model()->m_meshes[0].setStart(m_allVertices.count()*sizeof(Vertex));
                //Go through each vertex in this model and add it to the giant vector
                for (quint64 iii = 0; iii < creature->model()->m_meshes[0].size(); iii++)
                {
                    m_allVertices.append(creature->model()->m_meshes[0].modelVertices()[iii]);
                }
            }
        }
    }

    const GLint SIZEOF_Vertex = sizeof(Vertex);
    const GLint SIZEOF_GLfloat = sizeof(GLfloat);

    std::cout<<"Placed "<<
               m_allVertices.size()<<" vertices, and "<<
               m_allVertices.size()*SIZEOF_Vertex<<" data points."<<std::endl;

    m_program->bind();

    //Create the VAO and VBO and bind them both
    if (!m_VAO.isCreated())
    {
        m_VAO.create();
    }
    m_VAO.bind();

    if (!m_VBO.isCreated())
    {
        m_VBO.create();
    }
    m_VBO.bind();

    //Allocate the memory
    m_VBO.allocate(m_allVertices.constData(), m_allVertices.count()*SIZEOF_Vertex);
    m_VBO.setUsagePattern(QOpenGLBuffer::StaticDraw);

    std::cout<<"VBO size: "<<m_VBO.size()<<std::endl;

    m_program->enableAttributeArray(POSITION);
    GLint posOffset = 0;
    GLint posSize = 3;

    m_program->enableAttributeArray(TEX_POSITION);
    GLint texOffset = posSize;
    GLint texSize = 2;

    m_program->enableAttributeArray(TEX_ID);
    GLint tidOffset = posSize+texSize;
    GLint tidSize = 1;

    m_program->enableAttributeArray(COLOR);
    GLint colOffset = posSize+texSize+tidSize;
    GLint colSize = 3;

    m_program->enableAttributeArray(NORMAL);
    GLint norOffset = posSize+texSize+tidSize+colSize;
    GLint norSize = 3;

    GLint totalSize = (posSize+texSize+tidSize+colSize+norSize)*SIZEOF_GLfloat;

    m_program->setAttributeBuffer(POSITION,     GL_FLOAT, posOffset * SIZEOF_GLfloat, posSize, totalSize);
    m_program->setAttributeBuffer(TEX_POSITION, GL_FLOAT, texOffset * SIZEOF_GLfloat, texSize, totalSize);
    m_program->setAttributeBuffer(TEX_ID,       GL_FLOAT, tidOffset * SIZEOF_GLfloat, tidSize, totalSize);
    m_program->setAttributeBuffer(COLOR,        GL_FLOAT, colOffset * SIZEOF_GLfloat, colSize, totalSize);
    m_program->setAttributeBuffer(NORMAL,       GL_FLOAT, norOffset * SIZEOF_GLfloat, norSize, totalSize);

    //    std::cout<<m_program->log().toStdString()<<std::endl;
    //    m_VAO.release();
}

void RenderArea::buildModels()
{
    //    makeCurrent();

    bool missingModels = false;

    //TODO: make this more concise
    for (quint16 modelNumber=0; modelNumber<MAX_MODEL; modelNumber++)
    {
        switch (modelNumber) {
        case MODEL_WALL:
        {
            std::shared_ptr<Model> wallModel(new Model);
            Vertex newVertex;
            newVertex.color  = QVector3D(1.0f, 1.0f, 1.0f);
            newVertex.normal = QVector3D(0.0f, 0.0f, 0.0f);
            newVertex.texID  = TEX_WALL;

            ///Bottom
            {
                //Bottom left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(0.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f, 1.0f);
                wallModel->giveVertex(newVertex);
            }

            ///top
            {
                //Bottom left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(1.0f, 1.0f);
                wallModel->giveVertex(newVertex);
            }

            ///Left
            {
                //Bottom left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(      0.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(      1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(      0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(      1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(      0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(      1.0f, 1.0f);
                wallModel->giveVertex(newVertex);
            }

            ///Right
            {
                //Bottom left corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(      0.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(      1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(      0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(      1.0f, 0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(      0.0f, 1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(      1.0f, 1.0f);
                wallModel->giveVertex(newVertex);
            }

            ///Up
            {
                //Bottom left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(0.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f,       1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f,       1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
                newVertex.texPosition = QVector2D(1.0f,       1.0f);
                wallModel->giveVertex(newVertex);
            }

            ///Down
            {
                //Bottom left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(0.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f,       1.0f);
                wallModel->giveVertex(newVertex);

                //Bottom right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
                newVertex.texPosition = QVector2D(1.0f,       0.0f);
                wallModel->giveVertex(newVertex);

                //Top left corner
                newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(0.0f,       1.0f);
                wallModel->giveVertex(newVertex);

                //Top right corner
                newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
                newVertex.texPosition = QVector2D(1.0f,       1.0f);
                wallModel->giveVertex(newVertex);
            }

            wallModel->setType(MODEL_WALL);
            m_models.append(wallModel);

            break;
        }
        case MODEL_FLOOR:
        {
            std::shared_ptr<Model> floorModel(new Model);
            Vertex newVertex;
            newVertex.color  = QVector3D(1.0f, 1.0f, 1.0f);
            newVertex.normal = QVector3D(0.0f, 0.0f, 0.0f);
            newVertex.texID  = TEX_FLOOR;

            //Bottom left corner
            newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
            newVertex.texPosition = QVector2D(0.0f, 0.0f);
            floorModel->giveVertex(newVertex);

            //Bottom right corner
            newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
            newVertex.texPosition = QVector2D(1.0f, 0.0f);
            floorModel->giveVertex(newVertex);

            //Top left corner
            newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
            newVertex.texPosition = QVector2D(0.0f, 1.0f);
            floorModel->giveVertex(newVertex);

            //Bottom right corner
            newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
            newVertex.texPosition = QVector2D(1.0f, 0.0f);
            floorModel->giveVertex(newVertex);

            //Top left corner
            newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
            newVertex.texPosition = QVector2D(0.0f, 1.0f);
            floorModel->giveVertex(newVertex);

            //Top right corner
            newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
            newVertex.texPosition = QVector2D(1.0f, 1.0f);
            floorModel->giveVertex(newVertex);

            floorModel->setType(MODEL_FLOOR);
            m_models.append(floorModel);

            break;
        }
        case MODEL_PLAYER:
        {
            std::shared_ptr<Model> playerModel(new Model);
            buildCube(playerModel, TEX_PLAYER);

            playerModel->setType(MODEL_PLAYER);
            m_models.append(playerModel);

            break;
        }
        case MODEL_RAT:
        {
            std::shared_ptr<Model> ratModel(new Model);
            buildCube(ratModel, TEX_RAT);

            ratModel->setType(MODEL_RAT);
            m_models.append(ratModel);

            break;
        }
        case MODEL_MONKEY:
        {
            std::shared_ptr<Model> monkeyModel(new Model);
            buildCube(monkeyModel, TEX_MONKEY);

            monkeyModel->setType(MODEL_MONKEY);
            m_models.append(monkeyModel);

            break;
        }

        default:
        {
            missingModels=true;
            break;
        }
        }
    }

    if (missingModels)
    {
        std::cout<<"Warning: some models does not have a loader"<<std::endl;
    }
}

void RenderArea::buildCube(std::shared_ptr<Model> model, float texID)
{
    Vertex newVertex;
    newVertex.color  = QVector3D(1.0f, 1.0f, 1.0f);
    newVertex.normal = QVector3D(0.0f, 0.0f, 0.0f);
    newVertex.texID  = texID;

    ///Bottom
    {
        //Bottom left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(0.0f, 0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f, 1.0f);
        model->giveVertex(newVertex);
    }

    ///top
    {
        //Bottom left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f, 0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(1.0f, 1.0f);
        model->giveVertex(newVertex);
    }

    ///Left
    {
        //Bottom left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(      0.0f, 0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(      1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(      0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(      1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(      0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(      1.0f, 1.0f);
        model->giveVertex(newVertex);
    }

    ///Right
    {
        //Bottom left corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(      0.0f, 0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(      1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(      0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(      1.0f, 0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(      0.0f, 1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(      1.0f, 1.0f);
        model->giveVertex(newVertex);
    }

    ///Up
    {
        //Bottom left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(0.0f,       0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f,       0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f,       1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f,       0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f,       1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(1.0f, 0.0f, 1.0f);
        newVertex.texPosition = QVector2D(1.0f,       1.0f);
        model->giveVertex(newVertex);
    }

    ///Down
    {
        //Bottom left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(0.0f,       0.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f,       0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f,       1.0f);
        model->giveVertex(newVertex);

        //Bottom right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 0.0f);
        newVertex.texPosition = QVector2D(1.0f,       0.0f);
        model->giveVertex(newVertex);

        //Top left corner
        newVertex.position =    QVector3D(0.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(0.0f,       1.0f);
        model->giveVertex(newVertex);

        //Top right corner
        newVertex.position =    QVector3D(1.0f, 1.0f, 1.0f);
        newVertex.texPosition = QVector2D(1.0f,       1.0f);
        model->giveVertex(newVertex);
    }
}

void RenderArea::moveCamera(QVector2D movement)
{
    m_camera.translate(QVector3D(movement.x(), movement.y(), 0.0f));
    update();
}

void RenderArea::moveCamera(qint16 x, qint16 y)
{
    moveCamera(QVector2D(x, y));
}

void RenderArea::modifyZoom(qint8 amount)
{
    m_camera.modifyZoom(amount);
    update();
}

void RenderArea::setWorld(const std::shared_ptr<World> &world)
{
    m_world = world;
    if (!m_world.get())
    {
        return;
    }

    //Determine the vertices from this world
    determineModels();
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize RenderArea::sizeHint() const
{
    return QSize(200, 200);
}
























































































