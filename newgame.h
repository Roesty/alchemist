#ifndef NEWGAME_H
#define NEWGAME_H

#include <iostream>

#include <QObject>
#include <QDialog>
#include <QtWidgets>

QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
QT_END_NAMESPACE

class NewGame: public QDialog
{
    Q_OBJECT

private:
    QGridLayout* mainLayout;

    QLabel* characterNameLabel;
    QLineEdit* characterNameEntry;

    QLabel* classLabel;
    QComboBox* classSelect;
    QTextEdit* classDescription;

    QLabel* raceLabel;
    QComboBox* raceSelect;
    QTextEdit* raceDescription;

    QLabel* bioLabel;
    QTextEdit* bio;

    QDialogButtonBox *buttonBox;

    void closeEvent(QCloseEvent* event);

    void createSave(bool replaceExisting=false);

    enum eRaces {HUMAN, ELF, DWARF, MAX_RACE};
    enum eClasses {WARRIOR, MAGE, ROGUE, MAX_CLASS};

private slots:

    void checkInput();
    void showHelp();

public:
    NewGame();
};

#endif // NEWGAME_H
