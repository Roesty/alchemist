#include "limb.h"

QString Limb::name() const
{
    return m_attributes[NAME].toString();
}

void Limb::setName(const QString &name)
{
    m_attributes[NAME] = name;
}

qint32 Limb::hp() const
{
    return m_attributes[HP].toInt();
}

void Limb::setHp(const qint32 &hp)
{
    m_attributes[HP] = hp;
}

QVector<std::shared_ptr<Limb>> Limb::appendages() const
{
    return m_appendages;
}

void Limb::setToHeader()
{
    m_attributes.clear();
    m_attributes << "Name" << "HP";
}

Limb* Limb::parent() const
{
    return m_parent;
}

void Limb::setParent(const std::shared_ptr<Limb> &parent)
{
    m_parent = parent.get();
}

void Limb::setParent(Limb *parent)
{
    m_parent = parent;
}

Limb::Limb(QString name)
{
    m_appendages.clear();
    for (int attribute=0; attribute<MAX_LIMB_ATTRIBUTES; attribute++)
    {
        m_attributes << 0;
    }
    setName(name);
    setHp(100);

//    std::cout<<"Creating limb '"<<name.toStdString()<<"'."<<std::endl;
}

Limb::~Limb()
{
//    std::cout<<"Destroying "<<name().toStdString()<<std::endl;
    m_appendages.clear();
    if (m_parent)
    {
        m_parent = nullptr;
    }
    m_attributes.clear();
}

void Limb::attachLimb(QString name)
{
    std::shared_ptr<Limb> newLimb(new Limb(name));
    newLimb->setParent(this);

//    newLimb->setName(name);
    m_appendages.push_back(newLimb);
}

void Limb::attachLimb(std::shared_ptr<Limb> newLimb)
{
    newLimb->setParent(this);

    m_appendages.push_back(newLimb);
}

std::shared_ptr<Limb> Limb::appendage(qint32 row)
{
    return m_appendages.value(row);
}

Limb *Limb::appendagePtr(qint32 row)
{
//    std::cout<<"Limb '"<<name().toStdString()<<"' has "<<appendageCount()<<" appendages."<<std::endl;
//    std::cout<<"Trying to access #"<<row<<"."<<std::endl;

    return m_appendages[row].get();
}

qint32 Limb::appendageCount() const
{
//    std::cout<<"Appendage count for "<<name().toStdString()<<std::endl;
    return m_appendages.count();
}

QVariant Limb::data(int column) const
{
    return m_attributes.value(column);
}

int Limb::row() const
{
    /*
    std::cout<<"row of "<<name().toStdString()<<std::endl;
    if (m_parent)
    {
        return m_parent->appendages().indexOf(std::shared_ptr<Limb>(const_cast<Limb*>(this)));
    }
*/
    return 0;
}

qint32 Limb::columnCount() const
{
    return m_attributes.count();
}

QJsonObject Limb::serialized()
{
//    std::cout<<"serialized for limb: "<<name().toStdString()<<std::endl;
    //The json representing this limb
    QJsonObject json;

    //Set the name
    json["name"] = name();
    json["hp"] = hp();

    //Array for all the attached limbs
    QJsonArray appendages;

    //For each limb, retrieve the serialisation and store it
    for (std::shared_ptr<Limb> limb: m_appendages)
    {
        appendages.push_back(limb->serialized());
    }

    //Set the appendages
    json["appendages"] = appendages;

    //Return the completed json object
    return json;
}

void Limb::fromSerialized(QJsonObject json)
{
    //Set the name
    setName(json["name"].toString());
    setHp(json["hp"].toInt());

    //Get all the appendages
    QJsonArray appendages = json["appendages"].toArray();

    //Go through the appendages and store all of them
    for (int iii=0; iii<appendages.size(); iii++)
    {
        //Create a new limb
        std::shared_ptr<Limb> newLimb(new Limb());

        //Set the parent of the appendage to be the current limb
        newLimb->setParent(this);

        //Build this new limb for the json object in the appendages
        newLimb->fromSerialized(appendages[iii].toObject());

        m_appendages.push_back(newLimb);
    }
}


















































