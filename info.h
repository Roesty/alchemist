#ifndef INFO_H
#define INFO_H

#include <QWidget>
#include <QObject>
#include <QDialog>
#include <QtWidgets>

#include <memory>

#include "world.h"

class Info : public QWidget
{
    Q_OBJECT

    std::shared_ptr<World> m_world;

    QLabel* m_playerName;
    QLabel* m_timeLabel;
    QLineEdit* m_timeDisplay;

    QGridLayout* m_layout;

public:
    explicit Info(QWidget *parent = nullptr);

    void setWorld(const std::shared_ptr<World> &world);
    void updateDisplay();

signals:

public slots:
};

#endif // INFO_H
