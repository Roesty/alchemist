#include "world.h"

quint16 World::getW() const
{
    return m_w;
}

quint16 World::getH() const
{
    return m_h;
}

QVector<QString> World::getMessageLog() const
{
    return m_messageLog;
}

void World::writeToFile()
{
    if (m_fileName=="saves/load test.json")
    {
        return;
    }

    //    m_fileName.replace(".json", "TEST.json");

    //Create a file with this name
    QFile saveFile(m_fileName);

    //Open up the existing file with this name
    if (!saveFile.open(QIODevice::ReadOnly))
    {
        std::cout<<"Could not open file"<<std::endl;
        return;
    }

    QByteArray oldData = saveFile.readAll();

    QJsonDocument oldSave = QJsonDocument::fromJson(oldData);

    saveFile.close();

    // Useful for debugging save/load
    if (m_fileName=="saves/new game test.json")
    {
        saveFile.setFileName(QString(m_fileName).replace(".json", "TEST.json"));
    }

    saveFile.open(QIODevice::WriteOnly);

    ///Tile types
    //Choose the tile symbols
    QVector<QChar> symbols(2);
    symbols[false]='#';
    symbols[true]='.';

    //Store the tile symbols in the json document
    QJsonArray tileTypes;
    for (int symbol = 0; symbol < symbols.size(); ++symbol)
    {
        tileTypes.push_back(QJsonValue(symbols[symbol]));
    }

    ///Tile rows
    //Use the tile symbols to store the map
    QJsonArray tileRows;

    //Save it row by row. This makes it highly human-readable in the savefile created by Qt
    for (int y=0; y<m_h; y++)
    {
        //Row to hold the characters for this row on the map
        QString row="";
        for (int x=0; x<m_w; x++)
        {
            //Add the symbol of the model
            row += symbols[getTile(x, y)->isWalkable()];
        }
        //Push it to the front as we are working bottom up
        tileRows.push_front(QJsonValue(row));
    }

    ///Size information
    //Store the size information
    QJsonObject sizeInfo;
    sizeInfo["w"] = m_w;
    sizeInfo["h"] = m_h;

    ///Creatures
    QJsonArray creatures;
    for (std::shared_ptr<Creature> creature: m_creatures)
    {
        creatures.append(creature->serialized());
    }

    ///Gamelog
    QJsonArray log;
    for (QString event : m_messageLog)
    {
        log.append(QJsonValue(event));
    }

    //Put it all into the game json object
    QJsonObject newSave;
    newSave["time"]        = QJsonValue::fromVariant(getTime());
    newSave["name"]        = oldSave["name"];
    newSave["bio"]         = oldSave["bio"];
    newSave["last played"] = QDateTime::currentDateTime().toString(m_dateFormat);
    newSave["tiletypes"]   = tileTypes;
    newSave["tiles"]       = tileRows;
    newSave["size"]        = sizeInfo;
    newSave["creatures"]   = creatures;
    newSave["log"]         = log;

    QJsonDocument completedSave(newSave);

    saveFile.write(completedSave.toJson());
}

void World::loadFromFile(QString fileName)
{
    using namespace std;

    //Set the filename
    setFileName(fileName);

    //Open the file
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
    {
        cout<<"Could not open file"<<endl;
        return;
    }

    QByteArray saveData = file.readAll();

    QJsonDocument save = QJsonDocument::fromJson(saveData);

    if (save["new"].toBool())
    {
        cout<<"This save is new"<<endl;
        generateNew(500, 500, save);
        return;
    }

    cout<<"This save is not new"<<endl;

    //Collect the basic information
    QString playerName = save["name"].toString();
    QString playerBio  = save["bio"].toString();
    QString lastPlayed = save["last played"].toString();
    quint64 time       = save["time"].toVariant().toULongLong();

    //Set the time to the read value
    setTime(time);

    //Collect the size informations
    m_w = static_cast<quint16>(save["size"]["w"].toInt());
    m_h = static_cast<quint16>(save["size"]["h"].toInt());

    cout<<"Size of loaded map is: "<<m_w<<"x"<<m_h<<endl;

    //Collect the tile types
    QJsonArray tiletypes = save["tiletypes"].toArray();
    QJsonArray tiles = save["tiles"].toArray();

    //Load the rows bottom up as they are stored in reverse to make it more human readable
    for (int y_r=m_h-1; y_r>=0; y_r--)
    {
        int y = m_h-1-y_r;

        QString row = tiles[y_r].toString();
        for (int x=0; x<m_w; x++)
        {
            std::shared_ptr<Tile> newTile(new Tile);
            newTile->setPosition(x, y, 0.0f);

            QChar symbol = row[x];
            for (int tiletype=0; tiletype<tiletypes.size(); tiletype++)
            {
                if (tiletypes[tiletype].toString() == symbol)
                {
                    newTile->setIsWalkable(tiletype);
                    newTile->setID(y*m_w+x);
                }
            }

            m_tiles.push_back(newTile);
        }
    }

    QJsonArray creatures = save["creatures"].toArray();
    for (int iii=0; iii<creatures.size(); iii++)
    {
        //Create a new creature
        std::shared_ptr<Creature> newCreature(new Creature);

        //Load it from the serialised data
        newCreature->fromSerialized(creatures[iii].toObject());

        //Give the creature to the tile it belongs to
        m_tiles[newCreature->tileID()]->setCreature(newCreature);

        //Store the creature in the array of creatures
        m_creatures.push_back(newCreature);
    }

    //Insert all the messages to the log
    QJsonArray gameLog = save["log"].toArray();
    for (int iii=0; iii<gameLog.size(); iii++)
    {
        m_messageLog.push_front(gameLog[iii].toString());
    }
}

void World::setFileName(const QString &value)
{
    m_fileName = value;
    //    log("World filename is: "+m_fileName+".");
}

quint64 World::getTime() const
{
    return m_time;
}

void World::setTime(const quint64 &time)
{
    m_time = time;
}

QString World::getPlayerName()
{
    return getPlayer()->name();
}

void World::setPlayerName(const QString &playerName)
{
    getPlayer()->setName(playerName);
}

QString World::getPlayerBio()
{
    return getPlayer()->getBio();
}

void World::setPlayerBio(const QString &playerBio)
{
    getPlayer()->setBio(playerBio);
}

QVector<std::shared_ptr<Creature> > World::getCreatures() const
{
    return m_creatures;
}

std::shared_ptr<Creature> World::getCreature(qint32 index)
{
    return m_creatures[index];
}

void World::passTime()
{
    m_time++;
}

void World::act(std::shared_ptr<Creature> creature)
{
    switch (creature->AI())
    {
    case PLAYER_CONTROLLED:
    {
        std::cout<<"WARNING: PLAYER WAS SENT TO ACT METHOD"<<std::endl;
        break;
    }
    case DOCILE:
    {
        //Generate two random int values between -1 and 1
        GLint x = 0;
        GLint y = 0;
        while (x==0 && y==0)
        {
            x=qrand()%3-1;
            y=qrand()%3-1;
        }

        moveCreature(creature, x, y);

        break;
    }
    case MAX_AI_TYPES:
    {
        break;
    }
    }
}

void World::generateNew(quint16 w, quint16 h, QJsonDocument save)
{
    m_w=w;
    m_h=h;

    //Start the time at 0
    setTime(0);
    
    //Generate a random grid of bools
    for (quint16 y=0; y<m_h; y++)
    {
        for (quint16 x=0; x<m_w; x++)
        {
            std::shared_ptr<Tile> newTile(new Tile);

            if (x==0 || x==m_w-1 || y == 0 || y==m_h-1)
            {
                newTile->setIsWalkable(false);
            }
            else
            {
                newTile->setIsWalkable(qrand()%10);

                //                newTile->setIsWalkable(true);
            }

            newTile->setPosition(x, y, 0.0f);
            newTile->setID(y*m_w+x);

            m_tiles.push_back(newTile);
        }
    }
    log("Created new world of size "+QString::number(m_w)+"x"+QString::number(m_h)+".");
    std::cout<<"Created new world of size "<<
               QString::number(m_w).toStdString()<<"x"<<
               QString::number(m_h).toStdString()<<"."<<std::endl;

    // Generate the player creature
    std::shared_ptr<Creature> playerCreature(new Creature);

    playerCreature->setType(CREATURE_PLAYER);
    playerCreature->setName(save["name"].toString());
    playerCreature->setBio(save["bio"].toString());
    playerCreature->setAI(PLAYER_CONTROLLED);
    playerCreature->setID(0);
    playerCreature->buildCreature(CREATURE_PLAYER);

    m_creatures.push_back(playerCreature);

    //Place the player at the first tile that is walkable
    int tile = 0;
    while (!m_tiles[tile]->isWalkable())
    {
        tile = qrand()%getArea();
        //        tile++;
    }

    m_tiles[tile]->setCreature(playerCreature);

    for (int ratID=1; ratID<=10; ratID++)
    {
        //Generate a random monster
        std::shared_ptr<Creature> rat(new Creature);
        rat->setType(CREATURE_RAT);
        rat->setName("Rat");
        rat->setBio("A rat.");
        rat->setAI(DOCILE);
        rat->setID(ratID);
        rat->buildCreature(CREATURE_RAT);

        m_creatures.push_back(rat);

        //Place the player at the first tile that is walkable
        tile = 0;
        while (!m_tiles[tile]->canReceiveCreature())
        {
            tile = qrand()%getArea();
            //        tile++;
        }

        m_tiles[tile]->setCreature(rat);
    }
}

quint32 World::getArea()
{
    return m_w*m_h;
}

std::shared_ptr<Creature> World::getPlayer()
{
    for (int iii=0; iii<m_creatures.size(); iii++)
    {
        std::shared_ptr<Creature> creature = getCreature(iii);
        if (creature->isPlayer())
        {
            //            std::cout<<"player creature return from getPlayer has tileID of: "<<creature->tileID()<<std::endl;
            return creature;
        }
    }

    return nullptr;
}

std::shared_ptr<Tile> World::getTile(quint32 number)
{
    return m_tiles[number];
}

std::shared_ptr<Tile> World::getTile(quint16 x, quint16 y)
{
    return getTile(y*m_w+x);
}

void World::log(QString message)
{
    m_messageLog.append(message);
    emit messageLogged();
}

void World::moveCreature(std::shared_ptr<Creature> creature, QVector3D direction)
{
    qint32 tile = creature->tileID();
    //    log("TileID of creature: "+QString::number(tile));

    GLint x = 0;
    if (direction.x() > 0) { x =  1; }
    if (direction.x() < 0) { x = -1; }

    GLint y = 0;
    if (direction.y() > 0) { y =  1; }
    if (direction.y() < 0) { y = -1; }

    // The tile the player is trying to move to
    GLint nextTileIndex = tile+y*m_w+x;
    if (nextTileIndex < 0 || nextTileIndex >= getArea())
    {
        std::cout<<"Tile is out of bounds"<<std::endl;
        return;
    }

    //TODO: add check for horizontal wrapping
    //edges of the map are typically walled off, but still a risky contingency

    std::shared_ptr<Tile> currentTile = m_tiles[tile];
    std::shared_ptr<Tile> nextTile = m_tiles[nextTileIndex];

    //    std::cout<<"Moving from "<<tile<<" to "<<nextTileIndex<<"."<<std::endl;

    // If the tile is not walkable
    if (!nextTile->isWalkable())
    {
        //        std::cout<<"Tile is not walkable"<<std::endl;
        if (creature->isPlayer())
        {
            log("Cant walk there");
        }
        return;
    }
    // If the tile is occupied by another creature
    if (nextTile->creature().get())
    {
        //        std::cout<<"Tile is occupied"<<std::endl;
        if (creature->isPlayer())
        {
            log("Occupied tile. <attack not implemented>");
        }
        return;
    }

    //    log(creature->name()+" moved from "+QString::number(tile)+" to "+QString::number(nextTileIndex)+".");

    //Move the player from the previous tile to the next tile
    nextTile->setCreature(creature);
    currentTile->setCreature(nullptr);

    //Inform the creature that is was moved
    creature->hasMoved();
}

void World::moveCreature(std::shared_ptr<Creature> creature, GLfloat x, GLfloat y, GLfloat z)
{
    moveCreature(creature, QVector3D(x, y, z));
}

World::World()
{
    m_dateFormat = "HH:mm:ss dd/MM-yyyy";
}

























































