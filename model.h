#ifndef MODEL_H
#define MODEL_H

#include <QtOpenGL/QGL>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

#include <memory>
#include <iostream>

#include "mesh.h"

enum eModel
{
    MODEL_WALL=0,
    MODEL_FLOOR,
    MODEL_PLAYER,
    MODEL_RAT,
    MODEL_MONKEY,
    MAX_MODEL
};

//TODO: Split up model ID to types, or come up with another system

enum eTileModels
{

};

enum eCreatureModels
{

};

enum eTexture
{
    TEX_FLOOR=0,
    TEX_WALL,
    TEX_PLAYER,
    TEX_RAT,
    TEX_MONKEY,
    MAX_TEXTURE
};

class Model
{
private:
    QVector<Vertex> m_modelVertices;
    QVector<Vertex> m_vertices;
    QVector3D m_translation;

    eModel m_type;

public:
    Model();

    void giveVertex(Vertex newVertex);

    GLuint verticesSize() const;

    void bind();
    void release();

    QVector<Mesh> m_meshes;

    eModel type() const;
    void setType(const eModel &type);

    void applyTranslation();

    std::shared_ptr<Model> getCopy();

    QVector3D getTranslation() const;
    void setTranslation(const QVector3D &translation);
};

#endif // MODEL_H
