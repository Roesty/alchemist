#include "model.h"

QVector3D Model::getTranslation() const
{
    return m_translation;
}

void Model::setTranslation(const QVector3D &translation)
{
    m_translation = translation;
}

Model::Model()
{
    //This is the starting mesh
    m_meshes.append(Mesh());
}

void Model::giveVertex(Vertex newVertex)
{
    m_meshes[0].giveVertex(newVertex);
}

GLuint Model::verticesSize() const
{
    return m_vertices.size();
}

void Model::applyTranslation()
{
    m_meshes[0].applyTranslation(m_translation);
}

eModel Model::type() const
{
    return m_type;
}

void Model::setType(const eModel &type)
{
    m_type = type;
}

std::shared_ptr<Model> Model::getCopy()
{
    std::shared_ptr<Model> copy(new Model);
    for (int vertex=0; vertex<m_meshes[0].size(); vertex++)
    {
        copy->giveVertex(m_meshes[0].modelVertices()[vertex]);
    }

    copy->setType(type());

    return copy;
}
























































