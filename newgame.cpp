#include "newgame.h"

NewGame::NewGame()
{
    mainLayout=nullptr;
    characterNameEntry=nullptr;

    characterNameLabel = new QLabel("Character name:");
    characterNameEntry = new QLineEdit;
    characterNameEntry->setMaxLength(32);

    classLabel = new QLabel("Select class");
    classSelect = new QComboBox;
    classDescription = new QTextEdit;
    classDescription->setReadOnly(true);

    raceLabel = new QLabel("Select race");
    raceSelect = new QComboBox;
    raceDescription = new QTextEdit;
    raceDescription->setReadOnly(true);

    bioLabel = new QLabel("Character bio");
    bio = new QTextEdit;

    characterNameEntry->setText("Geralt of Rivia");
    bio->setText("Geralt of Rivia was a legendary witcher of the School of the Wolf active throughout the 13th century.");

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Help |
                                     QDialogButtonBox::Ok |
                                     QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(checkInput()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(buttonBox, SIGNAL(helpRequested()), this, SLOT(showHelp()));

    mainLayout = new QGridLayout;

    mainLayout->addWidget(characterNameLabel, 0, 0);
    mainLayout->addWidget(characterNameEntry, 1, 0);

    mainLayout->addWidget(classLabel, 2, 0);
    mainLayout->addWidget(classSelect, 3, 0);
    mainLayout->addWidget(classDescription, 4, 0);

    mainLayout->addWidget(raceLabel, 2, 1);
    mainLayout->addWidget(raceSelect, 3, 1);
    mainLayout->addWidget(raceDescription, 4, 1);

    mainLayout->addWidget(bioLabel, 5, 0, 1, 2);
    mainLayout->addWidget(bio, 6, 0, 1, 2);

    mainLayout->addWidget(buttonBox, 7, 0, 1, 2);

    setLayout(mainLayout);

}

void NewGame::closeEvent(QCloseEvent* event)
{
    std::cout<<"New Game Window was closed"<<std::endl;

    reject();
}

void NewGame::checkInput()
{
    QVector<QString> errors;

    if (characterNameEntry->text().isEmpty())
    {
        errors.push_back("You did not enter a name.");
    }

    if (errors.isEmpty())
    {
        createSave();
    }
    else
    {
        QMessageBox errorBox;
        for (int iii=0; iii<errors.size(); iii++)
        {
            errorBox.setText(errorBox.text()+"\n"+errors[iii]);
        }

        errorBox.exec();
    }
}

void NewGame::createSave(bool replaceExisting)
{
//    replaceExisting = true;

    std::cout<<"creating save"<<std::endl;

    //Collect the information from the form
    QString enteredName = characterNameEntry->text();
    QString enteredBio = bio->toPlainText();

    //Insert the data into a json object
    QJsonObject json;
    json["new"] = true;
    json["name"] = enteredName;
    json["bio"] = enteredBio;

    //TODO: Make the date format consistent with the world's
    json["last played"] = QDateTime::currentDateTime().toString("HH:mm:ss dd/MM-yyyy");

    //Determine the filename
    QString filename = "saves/"+json["name"].toString()+".json";

    //If the filename is already in use, find the first that is unique
    if (!replaceExisting)
    {
        quint8 uses = 0;
        while (QFileInfo::exists(filename))
        {
            uses+=1;
            filename = "saves/"+json["name"].toString()+QString::number(uses)+".json";
        }
    }

    //Create a savefile and open it
    QFile saveFile(filename);
    if (!saveFile.open(QIODevice::WriteOnly))
    {
        qWarning("Couldn't open save file.");
        return;
    }

    //Write the data to the file
    saveFile.write(QJsonDocument(json).toJson());

    //Give instruction for what to do next
    QMessageBox infoBox;
    infoBox.setText("Save created, load it from the main menu.");
    infoBox.exec();

    //close the window after the save was created
    accept();
}

void NewGame::showHelp()
{
    QMessageBox helpBox;
    helpBox.setText("Enter your character name and select the race and class. The bio is optional and purely cosmetic.");
    helpBox.setButtonText(QDialogButtonBox::Ok, "Thanks");
    helpBox.exec();
}































































