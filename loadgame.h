#ifndef LOADGAME_H
#define LOADGAME_H

#include <iostream>

#include <QObject>
#include <QDialog>
#include <QtWidgets>
#include <QFileInfo>
#include <QString>

QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
class QTableView;
class QStandardItemModel;
class QTreeView;
class QTreeWidget;
QT_END_NAMESPACE

class LoadGame: public QDialog
{
    Q_OBJECT

private:
    enum {FILE_NAME, CHAR_NAME, LAST_PLAYED, NEW, BIO, MAX_COLUMN};

    QGridLayout* mainLayout;

    std::shared_ptr<QStandardItemModel> saves;

    //TODO: upgrade to widget or create subclass. This is to get the selected index with the keyboard
    QTreeView* savesView;

    QDialogButtonBox* buttonBox;

    QLabel* selectedCharacterNameLabel;
    QLineEdit* selectedCharacterName;
    QLabel* selectedCharacterBioLabel;
    QTextEdit* selectedCharacterBio;

private slots:
    void showHelp();
    void loadSave();

    void saveSelected();

signals:
    void loadThisSave(QString fileName);

public:
    LoadGame();
};

#endif // LOADGAME_H
