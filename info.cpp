#include "info.h"

void Info::setWorld(const std::shared_ptr<World> &world)
{
    m_world = world;
    updateDisplay();
    if (m_world.get())
    {
        m_playerName->setText(m_world->getPlayerName());
    }
}

void Info::updateDisplay()
{
    if (m_world.get())
    {
        m_timeDisplay->setText(QString::number(m_world->getTime()));
    }
    else
    {
        m_timeDisplay->clear();
        m_playerName->clear();
    }
}

Info::Info(QWidget *parent) : QWidget(parent)
{
    m_playerName = new QLabel("");
    m_timeLabel = new QLabel("Time: ");
    m_timeDisplay = new QLineEdit();
    m_timeDisplay->setReadOnly(true);
    m_timeDisplay->setAlignment(Qt::AlignRight);
    m_timeDisplay->setMinimumHeight(18);

    m_layout = new QGridLayout();

    m_layout->addWidget(m_playerName,  0, 0, 1, 2);
    m_layout->addWidget(m_timeLabel,   1, 0, 1, 1);
    m_layout->addWidget(m_timeDisplay, 1, 1, 1, 1);

    setLayout(m_layout);

    show();
}
