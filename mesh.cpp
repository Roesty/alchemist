#include "mesh.h"

QVector<Vertex> Mesh::modelVertices() const
{
    return m_modelVertices;
}

void Mesh::giveVertex(quint32 mesh, Vertex newVertex)
{
    m_meshes[mesh].giveVertex(newVertex);
}

void Mesh::giveVertex(Vertex newVertex)
{
    m_modelVertices.append(newVertex);
}

QVector<Mesh> Mesh::meshes() const
{
    return m_meshes;
}

void Mesh::giveMesh(Mesh newMesh)
{
    m_meshes.push_back(newMesh);
}

quint64 Mesh::start() const
{
    return m_start;
}

void Mesh::setStart(const quint64 &start)
{
    m_start = start;
}

void Mesh::applyTranslation(QVector3D translation)
{
    m_vertices.clear();
    for (int vertex=0; vertex<m_modelVertices.size(); vertex++)
    {
        m_vertices.push_back(m_modelVertices[vertex]);
        m_vertices.back().position += translation;
    }
}

QVector<quint32> Mesh::indices() const
{
    return m_indices;
}

void Mesh::setIndices(const QVector<quint32> &indices)
{
    m_indices = indices;
}

quint64 Mesh::size()
{
    //TODO: implement recursive size calculation that includes child-meshes
    return m_modelVertices.size();
}

QVector<Vertex> Mesh::vertices() const
{
    return m_vertices;
}

Mesh::Mesh()
{

}
