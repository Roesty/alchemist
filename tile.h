#ifndef TILE_H
#define TILE_H

#include <QtOpenGL/QGL>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QFloat16>

#include <iostream>
#include <memory>

#include "object.h"
#include "creature.h"

enum eColor
{
    WALKABLE=0,
    UNWALKABLE,
    MAX_COLOR
};

enum eTextureDirection
{
    TEX_UP=0,
    TEX_RIGHT,
    TEX_DOWN,
    TEX_LEFT,
    MAX_ROTATION
};

class Tile: public Object
{
    quint32 m_ID;

    std::shared_ptr<Creature> m_creature;

    bool m_isWalkable;

    eTextureDirection m_textureRotation;

public:
    Tile();

    quint32 ID() const;
    void setID(const quint32 &ID);
    QJsonObject serialized();

    QMatrix4x4 getTranslationMatrix();

    bool isWalkable() const;
    void setIsWalkable(bool isWalkable);

    std::shared_ptr<Creature> creature() const;
    void setCreature(const std::shared_ptr<Creature> &creature);

    bool canReceiveCreature();
};

#endif // TILE_H













































