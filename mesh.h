#ifndef MESH_H
#define MESH_H

#include <QVector>
#include <QVector2D>
#include <QVector3D>

struct Vertex {

    //Position
    QVector3D position;

    //Texture position
    QVector2D texPosition;

    //Texture ID
    float texID;

    //Color
    QVector3D color;

    //Normal
    QVector3D normal;

/*
    bool operator==(Vertex other)
    {
        return
                this->position    == other.position    &&
                this->texPosition == other.texPosition &&
                this->texID       == other.texID       &&
                this->color       == other.color       &&
                this->normal      == other.normal;
    }
    */
};

enum eVertexAttribute
{
    POSITION=0,
    TEX_POSITION,
    TEX_ID,
    COLOR,
    NORMAL,
    MAX_VERTEX_ATTRIBUTE
};

class Mesh
{
private:
    QVector<Vertex> m_modelVertices;
    QVector<Vertex> m_vertices;
    QVector<quint32> m_indices;

    quint64 m_start;

    QVector<Mesh> m_meshes;

public:
    Mesh();

    QVector<Vertex> modelVertices() const;
    void giveVertex(quint32 mesh, Vertex newVertex);
    void giveVertex(Vertex newVertex);

    QVector<Mesh> meshes() const;
    void giveMesh(Mesh newMesh);

    quint64 start() const;
    void setStart(const quint64 &start);

    void applyTranslation(QVector3D translation);

    QVector<quint32> indices() const;
    void setIndices(const QVector<quint32> &indices);

    quint64 size();
    QVector<Vertex> vertices() const;
};

#endif // MESH_H
