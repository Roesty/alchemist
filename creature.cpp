#include "creature.h"

eCreatureType Creature::type() const
{
    return m_type;
}

void Creature::setType(const eCreatureType &type)
{
    m_type = type;
}

void Creature::translateModel(QVector3D translation)
{
    m_model->setTranslation(translation);
    m_model->applyTranslation();
}

void Creature::setZ(GLfloat z)
{
    m_position.setZ(z);
}

eAITypes Creature::AI() const
{
    return m_AI;
}

void Creature::setAI(const eAITypes &AI)
{
    m_AI = AI;
}

quint32 Creature::ID() const
{
    return m_ID;
}

void Creature::setID(const quint32 &ID)
{
    m_ID = ID;
}

QJsonObject Creature::serialized()
{
    QJsonObject json;
    json["name"]   = name();
    json["bio"]    = getBio();
    json["AI"]     = AI();
    json["ID"]     = QJsonValue::fromVariant(ID());
    json["tileID"] = QJsonValue::fromVariant(tileID());

    QJsonArray attributes;
    for (int attribute=0; attribute<MAX_CREATURE_ATTRIBUTES; attribute++)
    {
        attributes.push_back(getAttribute(eCreatureAttributes(attribute)));
    }
    json["attributes"] = attributes;
    json["type"]       = type();
    json["cooldown"]   = cooldown();
    json["body"]       = m_body->serialized();

    return json;
}

bool Creature::fromSerialized(QJsonObject json)
{
    setName(json["name"].toString());
    setBio(json["bio"].toString());
    setAI(eAITypes(json["AI"].toInt()));
    setID(json["ID"].toVariant().toUInt());
    setTileID(json["tileID"].toVariant().toUInt());

    QJsonArray attributes = json["attributes"].toArray();
    for (int attribute=0; attribute<MAX_CREATURE_ATTRIBUTES; attribute++)
    {
        setAttribute(eCreatureAttributes(attribute), attributes[attribute].toInt());
    }

    setType(eCreatureType(json["type"].toInt()));
    setCooldown(json["cooldown"].toInt());
    m_body = std::shared_ptr<Limb>(new Limb);
    m_body->fromSerialized(json["body"].toObject());

    return false;
}

quint32 Creature::tileID() const
{
    return m_tileID;
}

void Creature::setTileID(const quint32 &tileID)
{
    m_tileID = tileID;
}

QVector<qint32> Creature::getAttributes() const
{
    return m_attributes;
}

void Creature::setAttributes(const QVector<qint32> &attributes)
{
    m_attributes = attributes;
}

qint32 Creature::getAttribute(eCreatureAttributes attribute)
{
    return m_attributes[attribute];
}

void Creature::setAttribute(eCreatureAttributes attribute, qint32 value)
{
    m_attributes[attribute] = value;
}

qint32 Creature::cooldown() const
{
    return m_cooldown;
}

void Creature::setCooldown(const qint32 &cooldown)
{
    m_cooldown = cooldown;
}

void Creature::reduceCooldown()
{
    m_cooldown -= 1;
}

bool Creature::canMove()
{
    return cooldown()==0;
}

void Creature::hasMoved()
{
    setCooldown(getAttribute(MOVEMENT_SPEED));
}

QString Creature::name() const
{
    return m_name;
}

void Creature::setName(const QString &name)
{
    m_name = name;
}

QString Creature::getBio() const
{
    return m_bio;
}

void Creature::setBio(const QString &bio)
{
    m_bio = bio;
}

std::shared_ptr<Limb> Creature::getBody() const
{
    return m_body;
}

void Creature::setBody(const std::shared_ptr<Limb> &body)
{
    m_body = body;
}

bool Creature::isPlayer()
{
    return AI() == PLAYER_CONTROLLED;
}

Creature::Creature()
{
    for (int iii=0; iii<MAX_CREATURE_ATTRIBUTES; iii++)
    {
        m_attributes.push_back(0);
    }
    m_attributes[MOVEMENT_SPEED] = 10;
    setCooldown(0);

    setAI(PLAYER_CONTROLLED);
    m_body = nullptr;
}

void Creature::buildCreature(eCreatureType type)
{
    switch (type)
    {
    case CREATURE_PLAYER:
    {
        m_body = std::shared_ptr<Limb>(new Limb("Brain"));

        std::shared_ptr<Limb> head(new Limb("Head"));
        head->attachLimb("Right eye");
        head->attachLimb("Left eye");
        head->attachLimb("Nose");
        head->attachLimb("Mouth");
        head->attachLimb("Left ear");
        head->attachLimb("Right ear");

        std::shared_ptr<Limb> torso(new Limb("Torso"));
        torso->attachLimb("Heart");

        std::shared_ptr<Limb> rightArm(new Limb("Right arm"));
        rightArm->attachLimb("Thumb");
        rightArm->attachLimb("Pointer finger");
        rightArm->attachLimb("Middle finger");
        rightArm->attachLimb("Ring finger");
        rightArm->attachLimb("Little finger");

        std::shared_ptr<Limb> leftArm(new Limb("Left arm"));
        leftArm->attachLimb("Thumb");
        leftArm->attachLimb("Pointer finger");
        leftArm->attachLimb("Middle finger");
        leftArm->attachLimb("Ring finger");
        leftArm->attachLimb("Little finger");

        std::shared_ptr<Limb> rightLeg(new Limb("Right leg"));
        rightLeg->attachLimb("Big toe");
        rightLeg->attachLimb("Long toe");
        rightLeg->attachLimb("Middle toe");
        rightLeg->attachLimb("Fore toe");
        rightLeg->attachLimb("Little toe");

        std::shared_ptr<Limb> leftLeg(new Limb("Left leg"));
        leftLeg->attachLimb("Big toe");
        leftLeg->attachLimb("Long toe");
        leftLeg->attachLimb("Middle toe");
        leftLeg->attachLimb("Fore toe");
        leftLeg->attachLimb("Little toe");

        torso->attachLimb(rightArm);
        torso->attachLimb(leftArm);
        torso->attachLimb(rightLeg);
        torso->attachLimb(leftLeg);

        m_body->attachLimb(head);
        m_body->attachLimb(torso);

        break;
    }
    case CREATURE_RAT:
    {
        m_body = std::shared_ptr<Limb>(new Limb("Rat brain"));

        std::shared_ptr<Limb> head(new Limb("Head"));
        head->attachLimb("Right eye");
        head->attachLimb("Left eye");
        head->attachLimb("Nose");
        head->attachLimb("Mouth");
        head->attachLimb("Left ear");
        head->attachLimb("Right ear");

        std::shared_ptr<Limb> torso(new Limb("Torso"));

        std::shared_ptr<Limb> rightForeleg(new Limb("Right foreleg"));
        rightForeleg->attachLimb("Claws");

        std::shared_ptr<Limb> leftForeleg(new Limb("Left foreleg"));
        leftForeleg->attachLimb("Claws");

        std::shared_ptr<Limb> rightHindleg(new Limb("Right hindleg"));
        rightHindleg->attachLimb("Claws");

        std::shared_ptr<Limb> leftHindleg(new Limb("Left hindleg"));
        leftHindleg->attachLimb("Claws");

        break;
    }
    case CREATURE_MONKEY:
    {
        break;
    }
    case MAX_CREATURE:
    {
        break;
    }
    }
}





























