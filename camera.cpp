#include "camera.h"

Camera::Camera()
{
    m_curProjection=ORTHOGRAPHIC;

    m_w = 0.0f;
    m_h = 0.0f;
    m_near = 0.1f;
    m_far = 50000.0f;

    m_fovMin=5.0f;
    m_fovMax=150.0f;
    m_fov = 90.0f;
    //        m_fov = m_fovMax;

    m_moveSpeed=25.0f;

    m_zoomMin=10.0f;
    m_zoomMax=500.0f;

    m_zoom=80.0f;

    m_position = QVector3D(0, 0, -5);

    //yaw is initialized to -90.0 degrees since a yaw
    //of 0.0 results in a direction vector pointing to
    //the right so we initially rotate a bit to the left
}

Camera::eProjection Camera::getProjectionType()
{
    return m_curProjection;
}

QMatrix4x4 Camera::getProjection()
{
    switch (m_curProjection) {
    case PERSPECTIVE:
    {
        QMatrix4x4 projection;
        projection.ortho(-0.5f, +0.5f, +0.5f, -0.5f, 4.0f, 15.0f);
        projection.translate(m_position);
        return projection;
    }
    case ORTHOGRAPHIC:
    {
        m_curProjection = PERSPECTIVE;
        return getProjection();
    }
    }

    return m_projection;
}

void Camera::modifyZoom(GLfloat amount)
{
    //    std::cout<<"Modified with "<<amount<<" amount"<<std::endl;
    if (amount > 0)
    {
        m_zoom -= 10;
    }
    else
    {
        m_zoom += 10;
    }

    if (m_zoom < m_zoomMin)
    {
        m_zoom = m_zoomMin;
    }

    if (m_zoom > m_zoomMax)
    {
        m_zoom = m_zoomMax;
    }
}

void Camera::setPosition(const QVector3D &position)
{
    m_position = position;
}

QMatrix4x4 Camera::getView()
{
    QMatrix4x4 view;

    switch (m_curProjection)
    {
    case Camera::PERSPECTIVE:
    {
        view.perspective(60, m_w/m_h, 0.1f, 1000.0f);
        view.translate(m_position);
        view.translate(0, 0, -m_zoom);
        break;
    }
    case Camera::ORTHOGRAPHIC:
    {
        view.ortho(-m_w/m_zoom, m_w/m_zoom, -m_h/m_zoom, m_h/m_zoom, 0.0f, 15.0f);
        view.translate(m_position);
        break;
    }
    }

    return view;
}

void Camera::setOrthographic(QMatrix4x4 orthographic)
{
    m_orthographic=orthographic;
}

QVector3D Camera::getPosition()
{
    return m_position;
}

QVector3D Camera::getDirection()
{
    //Camera points directly into the screen
    return QVector3D(0.0f, 0.0f, -1.0f);
}

void Camera::translate(QVector3D translation)
{
    m_position += translation;
}

void Camera::recalculatePerspective()
{
}

void Camera::resetPerspective(float fov, float window_w, float window_h, float near, float far)
{
    m_fov = fov;
    m_w = window_w;
    m_h = window_h;
    m_near = near;
    m_far = far;
}

void Camera::updateWindowSize(GLuint window_w, GLuint window_h)
{
    m_w = window_w;
    m_h = window_h;
}

void Camera::moveInDirection(Camera::eDirection direction, float scalar)
{
    float distance = m_moveSpeed*scalar;

    switch (direction) {
    case FORWARD:
    {
        translate(getDirection()*distance);
        break;
    }
    case BACKWARD:
    {
        translate(-getDirection()*distance);
        break;
    }
    case RIGHT:
    {
        translate(-right()*distance);
        break;
    }
    case LEFT:
    {
        translate(right()*distance);
        break;
    }
    case UP:
    {
        translate(up()*distance);
        break;
    }
    case DOWN:
    {
        translate(-up()*distance);
        break;
    }
    }
}

void Camera::setX(GLfloat x)
{
    m_position.setX(x);
}

void Camera::setY(GLfloat y)
{
    m_position.setY(y);
}

void Camera::setZ(GLfloat z)
{
    m_position.setZ(z);
}

QVector3D Camera::forward()
{
    return QVector3D(0.0f, 0.0f, -1.0f);
}

QVector3D Camera::right()
{
    return QVector3D(1.0f, 0.0f, 0.0f);
}

QVector3D Camera::up()
{
    return QVector3D(0.0f, 1.0f, 0.0f);
}

void Camera::setToDefault()
{
}


































