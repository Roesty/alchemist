#include "object.h"

std::shared_ptr<Model> Object::model() const
{
    return m_model;
}

void Object::setModel(const std::shared_ptr<Model> &model)
{
    m_model = model;
//    m_model->translateVertices(m_position);
    m_model->setTranslation(position());
    m_model->applyTranslation();
}

QVector3D Object::position() const
{
    return m_position;
}

void Object::setPosition(const QVector3D &position)
{
    m_position = position;

    //Update the model as well
    if (model().get())
    {
        model()->setTranslation(m_position);
        model()->applyTranslation();
    }
}

void Object::setPosition(qfloat16 x, qfloat16 y, qfloat16 z)
{
    setPosition(QVector3D(x, y, z));
}

Object::Object()
{

}
