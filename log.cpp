#include "log.h"

void Log::setWorld(const std::shared_ptr<World> &world)
{
    m_world = world;
    m_log->clear();

    if (m_world.get())
    {
        readLog();

        connect(m_world.get(), SIGNAL(messageLogged()), this, SLOT(readMessage()));
    }
}

void Log::insertMessage(QString message)
{
    m_log->moveCursor(QTextCursor::Start);
    m_log->insertPlainText(message+"\n");
}

void Log::readMessage()
{
    m_log->moveCursor(QTextCursor::Start);
    m_log->insertPlainText(m_world->getMessageLog().back()+"\n");

    update();
}

void Log::setupWidgets()
{
    m_log = new QTextEdit();
    m_log->setReadOnly(true);

    m_mainLayout = new QGridLayout();

    m_mainLayout->addWidget(m_log, 0, 0);

    setLayout(m_mainLayout);
}

void Log::readLog()
{
    QVector<QString> messages = m_world->getMessageLog();
    for (int message=0; message<messages.size(); message++)
    {
        m_log->moveCursor(QTextCursor::Start);
        m_log->insertPlainText(messages[message]+"\n");
    }

    update();
}

Log::Log(QWidget *parent) : QWidget(parent)
{
    setupWidgets();

    show();
}
