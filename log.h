#ifndef LOG_H
#define LOG_H

#include <QWidget>

#include "world.h"
#include <memory>

class Log : public QWidget
{
    Q_OBJECT

    std::shared_ptr<World> m_world;

    void setupWidgets();

    QTextEdit* m_log;
    QGridLayout* m_mainLayout;

    void readLog();

public:
    explicit Log(QWidget *parent = nullptr);

    void setWorld(const std::shared_ptr<World> &world);
    void insertMessage(QString message);

signals:

public slots:
    void readMessage();
};

#endif // LOG_H
