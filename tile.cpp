#include "tile.h"

bool Tile::isWalkable() const
{
    return m_isWalkable;
}

void Tile::setIsWalkable(bool isWalkable)
{
    m_isWalkable = isWalkable;
}

std::shared_ptr<Creature> Tile::creature() const
{
    return m_creature;
}

void Tile::setCreature(const std::shared_ptr<Creature> &creature)
{
    m_creature = creature;

    if (m_creature.get())
    {
        //Give the new creature the position and ID of this tile
        m_creature->setPosition(position());
        m_creature->setTileID(ID());
    }
}

bool Tile::canReceiveCreature()
{
    //Ithas to be walkable
    if (!isWalkable())
    {
        return false;
    }

    //No creature can be here
    if (m_creature.get())
    {
        return false;
    }

    return true;
}

QMatrix4x4 Tile::getTranslationMatrix()
{
    QMatrix4x4 translation;
    translation.setToIdentity();
    translation.translate(position());
    return translation;
}

quint32 Tile::ID() const
{
    return m_ID;
}

void Tile::setID(const quint32 &ID)
{
    m_ID = ID;
}

QJsonObject Tile::serialized()
{
    QJsonObject json;
    json["ID"] = QJsonValue::fromVariant(ID());
    json["creatureID"] = QJsonValue::fromVariant(m_creature->ID());
    json["isWalkable"] = isWalkable();

    return json;
}

Tile::Tile()
{
    m_isWalkable=false;
    m_textureRotation=TEX_UP;
    m_creature = nullptr;
}
