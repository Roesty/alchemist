#ifndef ALCHEMIST_H
#define ALCHEMIST_H

#include <iostream>

#include <QObject>
#include <QDialog>
#include <QtWidgets>

#include <QApplication>
#include <QMainWindow>
#include <memory>

#include "world.h"
#include "info.h"
#include "renderarea.h"
#include "log.h"
#include "bodyview.h"

#include "newgame.h"
#include "loadgame.h"

QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
QT_END_NAMESPACE

class Alchemist: public QMainWindow
{
    Q_OBJECT

    enum eGameState
    {
        PLAYING=0,
        LOOK_AROUND
    };

    //Current input state
    eGameState m_state;

    //The world that is currently loaded
    std::shared_ptr<World> m_world;

    //The player controlled creature in the world
    std::shared_ptr<Creature> m_player;

    //If it is not the player's turn, he cannot give input to move the character
//    bool m_playerCanAct;

    //Displays basic player information
    Info* m_info;

    //Displays the recent events in the game
    Log* m_log;

    //Displays the game itself
    RenderArea* m_renderArea;

    //Displays the body of the player
    BodyView* m_bodyview;

    //The top menu, gives the player some extra controls
    QMenuBar* m_menu;
    QMenu* m_saveMenu;

    //Actions the user can perform
    QAction* m_newSave;
    QAction* m_loadSave;
    QAction* m_unloadSave;

    //Help menu
    QMenu* m_helpMenu;

    //Actions the user can perform
    QAction* m_getHelp;

    //Layout stuff
    QWidget* m_centralWidget;

    QGridLayout* m_leftPanel;
    QGridLayout* m_rightPanel;
    QGridLayout* m_layout;

    //Pointers to the windows to create and load games
    std::shared_ptr<NewGame> newGameWindow;
    std::shared_ptr<LoadGame> loadGameWindow;

    //Game loop
    void play();

    void setupGeneral();
    void setupMenu();
    void setupWindow();
    void setupConnections();

    void distributeWorld();

    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

    void movePlayer(QVector3D direction);
    void movePlayer(GLfloat x, GLfloat y, GLfloat z=0.0f);

public slots:
    void openNewGame();
    void openLoadGame();

    void enableWindow();
    void disableWindow();

    void loadGame(QString fileName);
    void unloadGame();

    void showHelp();

public:
    Alchemist();
    eGameState state() const;
    void setState(const eGameState &state);
};

#endif // ALCHEMIST_H
