#ifndef BODYVIEW_H
#define BODYVIEW_H

#include <QWidget>
#include <memory>
#include <QTreeView>

#include "world.h"
#include "bodymodel.h"

class BodyView : public QWidget
{
    Q_OBJECT

    std::shared_ptr<World> m_world;
    std::shared_ptr<Creature> m_player;

    QTreeView* m_view;
    std::shared_ptr<BodyModel> m_model;

    QGridLayout* m_mainLayout;
//    std::shared_ptr<QStandardItemModel> m_bodyData;

    void readPlayer();

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

public:
    explicit BodyView(QWidget *parent = nullptr);

    void setWorld(const std::shared_ptr<World> &world);

    std::shared_ptr<Creature> player() const;
    void setPlayer(const std::shared_ptr<Creature> &player);

signals:

public slots:
};

#endif // BODYVIEW_H
